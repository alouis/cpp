/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serialize.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 15:58:38 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 15:58:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serialize.hpp"

void    *serialize(void)
{
    std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
    char        *data;
    char        str1[9];
    char        str2[9];
    int         n;

    data = new char[sizeof(char *) * 2 + sizeof(int)];
    srand(time(NULL));
    n = rand() % 32767;
    for (int i = 0; i < 8; i++)
    {
        str1[i] = alphabet[rand() % 26];
        str2[i] = alphabet[rand() % 26];
    }
    str1[8] = '\0';                     //ending char *str with \0 to print them
    str2[8] = '\0';
    memcpy(data, str1, sizeof(char *));
    memcpy(data + sizeof(char *), &n, sizeof(int));
    memcpy(data + sizeof(char *) + sizeof(int), str2, sizeof(char *));
    std::cout << "---- Serialization of ----" << std::endl;
    std::cout << "1st char *: " << str1 << std::endl;
    std::cout << "int: " << n << std::endl;
    std::cout << "2nd char *: " << str2 << std::endl;

    return static_cast<void *> (data);
}

Data    *deserialize(void * raw)
{
    Data    *deserialized_data;

    deserialized_data = new Data;
    std::cout << "---- Deserialization ----" << std::endl;
    deserialized_data->s1 = std::string(static_cast<char *>(raw), 8);
    deserialized_data->n = *reinterpret_cast<int *>(static_cast<char*>(raw) + 8);
    deserialized_data->s2 = std::string(static_cast<char *>(raw) + 12, 8);
    return deserialized_data;
}

int main()
{
    void    *serialized_data;
    Data    *deserialized_data;

    serialized_data = serialize();
    std::cout << "Address of serialized data: " << serialized_data << std::endl;
    deserialized_data = deserialize(serialized_data);
    std::cout << "Deserialized str1 = " << deserialized_data->s1 << std::endl;
    std::cout << "Deserialized n = " << deserialized_data->n << std::endl;
    std::cout << "Deserialized str2 = " << deserialized_data->s2 << std::endl;
    delete static_cast<char *> (serialized_data);
    delete deserialized_data;
    return 0;
}