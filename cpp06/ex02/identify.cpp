/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   identify.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 16:00:50 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 17:23:48 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Classes.hpp"

Base * generate(void)
{
    int i;
    A   *a;
    B   *b;
    C   *c;

    srand(time(NULL));
    i = rand() % 3;
    if (i == 0)
    {
        std::cout << "Generating object of class A" << std::endl;
        a = new A;
        return a;
    }
    if (i == 1)
    {
        std::cout << "Generating object of class B" << std::endl;
        b = new B;
        return b;
    }
    std::cout << "Generating object of class C" << std::endl;
    c = new C;
    return c;
}

void identify_from_pointer(Base * p)
{
    if (dynamic_cast<A*>(p))
        std::cout << "object of class A" << std::endl;
    if (dynamic_cast<B*>(p))
        std::cout << "object of class B" << std::endl;
    if (dynamic_cast<C*>(p))
        std::cout << "object of class C" << std::endl;
}

void identify_from_reference( Base & p)
{
    try 
    {
        A &a = dynamic_cast<A &>(p);
        std::cout << "object of class A" << std::endl;
        (void)a;
    }
    catch (std::exception &e) {}
    try 
    {
        B &b = dynamic_cast<B &>(p);
        std::cout << "object of class B" << std::endl;
        (void)b;
    }
    catch (std::exception &e) {}
    try 
    {
        C &c = dynamic_cast<C &>(p);
        std::cout << "object of class C" << std::endl;
        (void)c;
    }
    catch (std::exception &e) {}
}

int main()
{
    Base *random_class;

    random_class = generate();
    identify_from_pointer(random_class);
    identify_from_reference(*random_class);
    delete random_class;
    return 0;
}