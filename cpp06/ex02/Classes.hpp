/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Classes.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 16:00:55 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 16:01:32 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLASSES_HPP
# define CLASSES_HPP

# include <iostream>
# include <string>

class Base { 
    public: 
        virtual ~Base(void) {} 
};

class A : public Base {};

class B : public Base {};

class C : public Base {};

#endif