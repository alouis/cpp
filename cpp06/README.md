## Casts

# Useful links

 * [Valeur littérale (chaîne et caractère)](https://docs.microsoft.com/fr-fr/cpp/cpp/string-and-character-literals-cpp?view=msvc-160)
 * [Casting Operators](https://www.tutorialspoint.com/cplusplus/cpp_casting_operators.htm)
 * [Typecasting](https://www.cplusplus.com/doc/oldtutorial/typecasting/)
 * [Why static and not dynamic_cast with non-polymorphic types](https://stackoverflow.com/questions/4644753/c-dynamic-cast-polymorphic-requirement-and-downcasting#4644856)
 * [Virtual inheritance](https://en.wikipedia.org/wiki/Virtual_inheritance)
 * [Limits](https://docs.microsoft.com/en-us/cpp/c-language/limits-on-floating-point-constants?view=msvc-160)
 * [NaN, inf, -inf](https://docs.oracle.com/cd/E37483_01/server.751/es_eql/src/ceql_literals_nan.html)
 * [Identify child class with pointer](https://stackoverflow.com/questions/40839836/identifying-what-child-class-object-a-base-class-pointer-is-pointing-at)
