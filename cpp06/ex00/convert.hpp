/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 15:46:42 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 15:53:36 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONVERT_HPP
# define CONVERT_HPP

# include <iostream>
# include <string>
# include <cstdlib>
# include <limits>
# include "float.h"

typedef enum    e_type
{
    UNKNOWN, CHAR, INT, FLOAT, DOUBLE
}               t_type;

void    display_char(char *arg);
void    display_int(char *arg);
void    display_float(char *arg);
void    display_double(char *arg);

#endif