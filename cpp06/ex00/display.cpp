/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 15:46:44 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 15:54:26 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "convert.hpp"

int     exception_float(char *arg)
{
    std::string exc(arg);

    if (!exc.compare("-inff") || !exc.compare("+inff") || !exc.compare("nanf"))
    {
        std::cout << "char: impossible" << std::endl;
        std::cout << "int: impossible" << std::endl;
        std::cout << "float: " << exc << std::endl;
        std::cout << "double: " << exc.erase(exc.size() - 1, 1) << std::endl;
        return 1;
    }
    return 0;
}

int     exception_double(char *arg)
{
    std::string exc(arg);

    if (!exc.compare("-inf") || !exc.compare("+inf") || !exc.compare("nan"))
    {
        std::cout << "char: impossible" << std::endl;
        std::cout << "int: impossible" << std::endl;
        std::cout << "float: " << exc + 'f' << std::endl;
        std::cout << "double: " << exc << std::endl;
        return 1;
    }
    return 0;
}

void    display_char(char *arg)
{
    char c;

    c = arg[0];
    std::cout << "char: " << c << std::endl;
    std::cout << "int: " << static_cast<int> (c) << std::endl;
    std::cout << "float: " << static_cast<float> (c) << std::endl;
    std::cout << "double: " << static_cast<double> (c) << std::endl;
}

void    display_int(char *arg)
{
    float i;

    i = std::stof(arg);
    if (i < 32 || i > 126)
        std::cout << "char: Non displayable" << std::endl;
    else
        std::cout << "char: " << static_cast<char> (i) << std::endl;
    if (i < INT8_MIN || i > INT8_MAX)
        std::cout << "int: overflow" << std::endl;
    else
        std::cout << "int : " << i << std::endl;
    std::cout << "float: " << static_cast<float> (i) << std::endl;
    std::cout << "double: " << static_cast<double> (i) << std::endl;
}

void    display_float(char *arg)
{
    float f;

    if (exception_float(arg))
        return ;
    f = std::stof(arg);
    if (static_cast<int> (f) < 32 || static_cast<int> (f) > 126)
        std::cout << "char: Non displayable" << std::endl;
    else
        std::cout << "char: " << static_cast<char> (f) << std::endl;
    if (f < INT8_MIN || f > INT8_MAX)
        std::cout << "int: overflow" << std::endl;
    else
        std::cout << "int : " << static_cast<int> (f) << std::endl;
    std::cout << "float: " << f << std::endl;
    if (f < DBL_MIN || f > DBL_MAX)
        std::cout << "double: overflow" << std::endl;
    else
        std::cout << "double: " << static_cast<double> (f) << std::endl;
}

void    display_double(char *arg)
{
    double d;

    if (exception_double(arg))
        return ;
    d = std::stod(arg);
    if (static_cast<int> (d) < 32 || static_cast<int> (d) > 126)
        std::cout << "char: Non displayable" << std::endl;
    else
        std::cout << "char: " << static_cast<char> (d) << std::endl;
    if (d < INT8_MIN || d > INT8_MAX)
        std::cout << "int: overflow" << std::endl;
    else
        std::cout << "int : " << static_cast<int> (d) << std::endl;
    std::cout << "float: " << static_cast<float> (d) << std::endl;
    std::cout << "double: " << d << std::endl;
}