/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 15:46:40 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 15:46:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "convert.hpp"

t_type get_type(char *arg)
{
    std::string var(arg);
    int         d;
    int         i;

    d = 0;
    i = 0;
    if (var.size() == 1 && !isdigit(var[0]))
        return CHAR;
    if (!var.compare("-inff") || !var.compare("+inff") || !var.compare("nanf"))
        return FLOAT;
    if (!var.compare("-inf") || !var.compare("+inf") || !var.compare("nan"))
        return DOUBLE;
    if (!isdigit(var[0]) && var.at(0) != '+' && var.at(0) != '-')
        return UNKNOWN;
    if (var[0] == '+' || var[0] == '-')
        i = 1;
    while (var[i] != '\0')
    {
        if (!isdigit(var[i]))
        {
            if (var[i] == '.' && d == 0)
                d = 1;
            else if (var[i] == 'f' && i == (var.size() - 1) && d == 1)
                return FLOAT;
            else
                return UNKNOWN;
        }
        i++;
    }
    if (d == 1)
        return DOUBLE;
    return INT;
}

int main(int argc, char **argv)
{
    t_type  type;
    void    (*display_conversion[4])(char *arg) = {
        display_char,
        display_int,
        display_float,
        display_double,
    };

    if (argc != 2 || !(type = get_type(argv[1])))
    {
        std::cout << "Error: converting only one char, int, float or double." << std::endl;
        return 1;
    }
    display_conversion[type - 1](argv[1]);
    return 0;
}