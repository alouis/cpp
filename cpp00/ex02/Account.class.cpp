/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:13:13 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:13:16 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Account.class.hpp"
#include <string>
#include <iostream>

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;

Account::Account(int initial_deposit)
{
    _accountIndex = _nbAccounts;
    _amount = initial_deposit;
	_nbDeposits = 0;
	_nbWithdrawals = 0;
    _nbAccounts += 1;
    _totalAmount += initial_deposit;

    std::cout << "index:" << _accountIndex << ";";
    std::cout << "amount:" << _amount << ";";
    std::cout << "created" << std::endl;
}

Account::~Account() 
{
    std::cout << "index:" << _accountIndex << ";";
    std::cout << "amount:" << _amount << ";";
    std::cout << "closed" << std::endl;
}

void    Account::displayAccountsInfos()
{
    std::cout << "accounts:" << getNbAccounts() << ";";
    std::cout << "total:" << getTotalAmount() << ";";
    std::cout << "deposits:" << getNbDeposits() << ";";
    std::cout << "withdrawals:" << getNbWithdrawals() << ";" << std::endl;
}

void    Account::displayStatus()
{
    std::cout << "index:" << _accountIndex << ";";
    std::cout << "amount:" << _amount << ";";
    std::cout << "deposits:" << _nbDeposits << ";";
    std::cout << "withdrawals:" << _nbWithdrawals << ";" << std::endl;
}

void    Account::makeDeposit(int deposit)
{
    std::cout << "index:" << _accountIndex << ";";
    std::cout << "p_amount:" << _amount << ";";
    std::cout << "deposit:" << deposit << ";";
    _amount += deposit;
    _totalAmount += deposit;
    std::cout << "amount:" << _amount << ";";
    _nbDeposits +=1;
    _totalNbDeposits += 1;
    std::cout << "nb_deposits:" << _nbDeposits << ";" << std::endl;
}


int		Account::checkAmount()
{
    if (_amount > 50)
        return (1);
    else
        return (0);
}

bool	Account::makeWithdrawal(int withdrawal)
{
    std::cout << "index:" << _accountIndex << ";";
    std::cout << "p_amount:" << _amount << ";";
    if (checkAmount())
    {
        std::cout << "withdrawal:" << withdrawal << ";";
        _amount -= withdrawal;
        _totalAmount -= withdrawal;
        std::cout << "amount:" << _amount << ";";
        _nbWithdrawals += 1;
        _totalNbWithdrawals += 1;
        std::cout << "nb_withdrawals:" << _nbWithdrawals << ";";
    }
    else
        std::cout << "withdrawal:refused";
    std::cout << std::endl;
    return (0);
}