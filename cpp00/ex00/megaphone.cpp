/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:12:16 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:12:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>

int main(int argc, char **argv)
{
    int i;
    std::string sentence;
    
    if (argc == 1)
        std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
    else
    {
        for (i=1; i<argc; i++)
            sentence = sentence + ' ' + argv[i];
        for (i=0; i<sentence.size(); i++)
            putchar(toupper(sentence[i]));
        std::cout << std::endl;
    }
    return 0;
}