/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:12:25 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:12:32 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

s#include "Contact.hpp"

Contact::Contact()
{
    infos[0] = "first name";
    infos[1] = "last name";
    infos[2] = "nickname";
    infos[3] = "login";
    infos[4] = "postal address";
    infos[5] = "email address";
    infos[6] = "phone number";
    infos[7] = "birthday date";
    infos[8] = "favorite meal";
    infos[9] = "underwear color";
    infos[10] = "darkest secret";
}

void    Contact::get_infos()
{
    int         i;
    std::string answer;

    i = 0;
    answer.clear();
    while (i < 11)
    {
        std::cout << "What's your " << infos[i] << "?" << std::endl;
        while (answer.empty())
        {
            getline(std::cin, answer);
            infos[i] = answer;
        }        
        answer.clear();
        i++;
    }
}
