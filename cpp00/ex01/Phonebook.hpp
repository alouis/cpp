/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:13:04 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:13:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

# include <string>
# include <iostream>
# include <iomanip>
# include "Contact.hpp"

class Phonebook
{
    public:
        int     nb;
        void    add_contact();
        void    search();
        
    private:
        void    display_preview();
        void    display_contact(int index);
        Contact contact[8];
};

#endif