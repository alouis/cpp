/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:12:58 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:13:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Phonebook.hpp"

void    Phonebook::add_contact()
{
    if (nb > 7)
    {
        std::cout << "There are 8 contacts in the phonebook, it's full." << std::endl;
        return ;
    }
    contact[nb].get_infos();
    nb++;
}

void    Phonebook::display_preview()
{
    int         ct;
    int         i;
    std::string info;

    for (ct=0; ct<nb; ct++)
    {
        std::cout << std::setw(10) << ct + 1 << "|";
        for (i=0; i<3; i++)
        {
            info = contact[ct].infos[i];
            if (info.size() < 11)
                std::cout << std::setw(10) << info << "|";
            else
                std::cout << std::setw(9) << info.substr(0, 9) << ".|";
        }
        std::cout << std::endl;
    }
}

void    Phonebook::display_contact(int index)
{
    int i;
    
    if (index <= 0 || index > nb)
    {
        std::cout << "This contact doesn't exist." << std::endl;
        return ;
    }
    index--;
    for (i=0; i<11; i++)
        std::cout << contact[index].infos[i] << std::endl;
}

void    Phonebook::search()
{
    int         i;
    int         idx;
    std::string info;
    const char  *nbr;

    if (nb == 0)
    {
        std::cout << "You haven't saved any contact yet." << std::endl;
        return ;
    }
    display_preview();
    std::cout << "Which contact's informations do you want to see ?" << std::endl;
    std::cin >> info;
    nbr = info.c_str();
    idx = atoi(nbr);
    display_contact(idx);
}