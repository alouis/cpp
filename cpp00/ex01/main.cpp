/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 16:12:44 by alouis            #+#    #+#             */
/*   Updated: 2021/03/29 16:12:45 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Phonebook.hpp"
#include "Contact.hpp"

int main()
{
    std::string command;
    Phonebook   phonebook;

    phonebook.nb = 0;
    while (1)
    {
        std::cout << "Do you want to look for a contact (SEARCH), add a contact (ADD) or exit the program (EXIT) ?" << std::endl;
        while (command.empty())
            getline(std::cin, command);
        if (command == "ADD")
            phonebook.add_contact();
        else if (command == "SEARCH")
            phonebook.search();
        else if (command == "EXIT")
            break ;
        command.clear();
    }
    return (0);
}