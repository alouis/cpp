/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:03:13 by alouis            #+#    #+#             */
/*   Updated: 2021/04/20 13:29:41 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITER_HPP
# define ITER_HPP

template <typename T>
void    iter(T* array, int size, void(*f)(T&))
{
    for (int i = 0; i < size; i++)
        f(array[i]);
}

#endif