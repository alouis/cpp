/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:03:09 by alouis            #+#    #+#             */
/*   Updated: 2021/04/20 13:29:56 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iter.hpp"
#include <iostream>
#include <string>
#include <math.h>

void power_of_5(int &n)
{
    std:: cout << pow(n, 5) << std::endl;
}

void to_lower(char &c)
{
    char l;

    if (c > 64 && c < 91)
    {
        l = c + 32;
        std::cout << l << std::endl;
    }
    else
        std::cout << c << std::endl;
}

template <typename T>
void print_element(T &element)
{
    std::cout << "** " << element << " **" << std::endl;
}

int main()
{
    int         numbers[] = {1, 5, 7, 9, 11, 20};
    std::string string[] = {"Pagina", "hac", "domestica", "certior", "fies"};
    char        caps[12] = {'H', 'E', 'L', 'L', 'L', 'O', ' ', 'W', 'O', 'R', 'L', 'D'};

    std::cout << "Printing elements of array" << std::endl;
    iter(numbers, 6, print_element);
    std::cout << std::endl;
    iter(string, string->size() - 1, print_element);
    std::cout << std::endl;
    iter(caps, 12, print_element);

    std::cout << std::endl;
    std::cout << "Numbers to the power of 5" << std::endl;
    iter(numbers, 6, power_of_5);

    std::cout << std::endl;
    std::cout << "Caps to lower case" << std::endl;
    iter(caps, 12, to_lower);
    
    return 0;
}