/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:01:24 by alouis            #+#    #+#             */
/*   Updated: 2021/04/21 08:24:17 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "whatever.hpp"
#include <iostream>

int main(void)
{
    int a;
    int b;

    a = 25;
    b = -2;
    std::cout << "a = " << a << ", b = " << b << std::endl;

    int minimum;
    minimum = ::min<int>(a, b);
    std::cout << "mininum = " << minimum << std::endl;

    int maximum;
    maximum = ::max<int>(a, b);
    std::cout << "maximum = " << maximum << std::endl;
    
    ::swap<int>(a, b);
    std::cout << "- Swaping integers\na = " << a << ", b = " << b << std::endl;

    std::string s1("hello");
    std::string s2("world");
    std::string str_min = ::min(s1, s2);
    std::string str_max = ::max(s1, s2);

    std::cout  << std::endl << "Minimum string : " << str_min << std::endl;
    std::cout  << "Maximum string : " << str_max << std::endl;

    std::cout << "s1 = " << s1 << ", s2 = " << s2 << std::endl;
    ::swap(s1, s2);
    std::cout << "- Swaping strings\ns1 = " << s1 << ", s2 = " << s2 << std::endl;

    double f;
    double g;

    f = -0.078276;
    g = 0.156729;
    std::cout << std::endl << "f = " << f << ", g = " << g << std::endl;
    std::cout << "mininum = " << ::min(f, g) << std::endl;
    std::cout << "maximum = " << ::max(f, g) << std::endl;
    ::swap(f, g);
    std::cout << "- Swaping double\nf = " << f << ", g = " << g << std::endl;
    return 0;
}