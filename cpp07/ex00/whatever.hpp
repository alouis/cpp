/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:02:52 by alouis            #+#    #+#             */
/*   Updated: 2021/04/20 13:29:05 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WHATEVER_HPP
# define WHATEVER_HPP

template <typename T>
void    swap(T& a, T& b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

template <typename T>
T       max(const T& a, const T& b)
{
    if (a == b)
        return a;
    return a > b ? a : b;
}

template <typename T>
T       min(const T& a, const T& b)
{
    if (a == b)
        return a;
    return a < b ? a : b;
}

#endif