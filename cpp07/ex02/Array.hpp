/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:04:01 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 17:04:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP

#include <iostream>

template <class T>
class Array
{
    public:
        Array() : _array(NULL), _size(0) 
        {

        }

        Array(unsigned int n) : _size(n)
        {
            _array = new T[_size];
        }

        ~Array()
        {
            delete[] _array;
        }

        Array (const Array<T> &to_copy) : _size(to_copy.size)
        {
                *this = to_copy;
        }

        Array& operator = (Array<T> &to_copy)
        {
            _size = to_copy.size();
            if (_size)
            {
                _array = new T[_size];
                for (int i = 0; i < _size; i++)
                    _array[i] = to_copy._array[i];
            }
            return *this;
        }

        unsigned int    size()
        {
            return _size;
        }

        class OutOfRangeException : public std::exception
        {
            const char * what () const throw ()
            {
                return "Error: index out of range";
            }
        };

        T& operator[](unsigned int index)
        {
            if (index < 0 || index > _size)
                throw OutOfRangeException();
            return _array[index];
        }
        
        const T& operator[](unsigned int index) const
        {
            if (index < 0 || index > _size)
                throw OutOfRangeException();
            return _array[index];
        }
    
    private:
        T               *_array;
        unsigned int    _size;
};

#endif