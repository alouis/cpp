/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 17:03:59 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 17:04:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"
#include <iostream>
#include <string>

int main()
{
    Array<const int> empty_array;
    Array<int>  numbers(9);
    Array<char> letters(5);
    Array<std::string> string(5);
    Array<std::string> copy_string;
    
    copy_string = string;
    
    for (int i = 0; i < numbers.size(); i++)
        numbers[i] = i;
    letters[0] = 'w';
    letters[1] = 'o';
    letters[2] = 'r';
    letters[3] = 'l';
    letters[4] = 'd';

    string[0] = "Aurea";
    string[1] =  "prima";
    string[2] =  "sata";
    string[3] =  "est";
    string[4] =  "aetas";

    copy_string = string;

    std::cout << "-> Array initialized without size:\nsize = " << empty_array.size() << std::endl;

    std::cout << std::endl;
    std::cout << "-> Array of int intitialized with size 9:\nsize = " << numbers.size() << std::endl;
    try
    {
        std::cout << "Subscrit operator with index out of range:" << std::endl;
        numbers[-3] = -3;
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        std::cout << "-> Subscrit operator with index in range:" << std::endl;
        numbers[3] = -3;
        for (int i = 0; i < numbers.size(); i++)
            std::cout << numbers[i] << std::endl;
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }

    std::cout << std::endl;
    std::cout << "-> Array of char intitialized with size 5:\nsize = " << letters.size() << std::endl;
    try
    {
        std::cout << "-> Subscrit operator with index out of range:" << std::endl;
        letters[-3] = '!';
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        std::cout << "-> Subscrit operator with index in range:" << std::endl;
        letters[1] = '*';
        for (int i = 0; i < letters.size(); i++)
            std::cout << letters[i] << std::endl;
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }

    std::cout << std::endl;
    std::cout << "-> Array of std::string intitialized with size 5:\nsize = " << string.size() << std::endl;
    try
    {
        std::cout << "-> Subscrit operator with index out of range:" << std::endl;
        string[-3] = "φαύλως";
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        std::cout << "-> Subscrit operator with index in range:" << std::endl;
        string[1] = "φαύλως";
        for (int i = 0; i < string.size(); i++)
            std::cout << string[i] << std::endl;
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }

    std::cout << std::endl;
    std::cout << "-> Copy of array std::string before change:" << std::endl;
    for (int i = 0; i < copy_string.size(); i++)
        std::cout << copy_string[i] << std::endl;
    
    return 0;
}