### Templates

# Useful links 
 * [Template class in C++](https://www.geeksforgeeks.org/templates-cpp/)
 * [Openclassroom (FR)](https://openclassrooms.com/en/courses/1894236-programmez-avec-le-langage-c/1903999-creez-des-templates)
 * [Defining and using templates](https://docs.microsoft.com/en-us/cpp/cpp/templates-cpp?view=msvc-160)
 * [Fonctions et classes templates (FR)](https://fr.wikibooks.org/wiki/Programmation_C-C%2B%2B/Les_templates/Fonctions_et_classes_template)
 * [Overloading [] operator](https://www.geeksforgeeks.org/overloading-subscript-or-array-index-operator-in-c/)