/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:40:26 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:40:26 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(std::string const name, int grade) : b_name(name)
{
    try 
    {
        if (grade < 1)
        {
            b_grade = 150;
            throw GradeTooHighException();
        }
        if (grade > 150)
        {
            b_grade = 150;
            throw GradeTooLowException();
        }
        else
            b_grade = grade;
    }
    catch (std::exception& exception) 
    {
        std::cerr << exception.what() << std::endl;
    }
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat::Bureaucrat (const Bureaucrat& b) : b_name(b.getName())
{
    try 
    {
        if (b.getGrade() < 1) 
            throw GradeTooHighException();
        if (b.getGrade() > 150) 
            throw GradeTooLowException();
        else
            b_grade = b.getGrade();
    }
    catch (std::exception& exception) 
    {
        std::cerr << exception.what() << std::endl;
    }
}

Bureaucrat& Bureaucrat::operator=(const Bureaucrat& b)
{
    if (this != &b)
        this->b_grade = b.b_grade;
    return *this;
}

const char * Bureaucrat::GradeTooHighException::what () const throw ()
{
    return "Error : grade too high";
}

const char * Bureaucrat::GradeTooLowException::what () const throw ()
{
    return "Error : grade too low";
}


std::string const   Bureaucrat::getName() const
{
    return b_name;
}

int                 Bureaucrat::getGrade() const
{
    return b_grade;
}

void                Bureaucrat::incrementGrade()
{
    try
    {
        if (b_grade - 1 <= 0)
            throw GradeTooHighException();
        else
            b_grade -= 1;
    }
    catch (std::exception& exception) 
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Bureaucrat::decrementGrade()
{
    try
    {
        if (b_grade + 1 > 150)
            throw GradeTooLowException();
        else
            b_grade += 1;
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }
}

std::ostream&   operator<<(std::ostream& os, const Bureaucrat& b)
{
    os << b.getName() << ", bureaucrat grade " << b.getGrade() << std::endl;
    return os;
}