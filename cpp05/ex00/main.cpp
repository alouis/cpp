/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:40:21 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:40:22 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

int main()
{
    Bureaucrat young_bureaucrat("Ernest", 200);
    Bureaucrat old_bureaucrat("Allie", 1);

    std::cout << young_bureaucrat;
    std::cout << old_bureaucrat;
    old_bureaucrat.incrementGrade();
    std::cout << old_bureaucrat;
    young_bureaucrat.decrementGrade();
    std::cout << young_bureaucrat;
    old_bureaucrat.decrementGrade();
    std::cout << old_bureaucrat;
    young_bureaucrat.incrementGrade();
    std::cout << young_bureaucrat;

    return 0;
}