/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:40:56 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:40:57 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include <iostream>
# include <string>
# include <ostream>
# include <exception>
# include "Form.hpp"

class Form;

class Bureaucrat
{
    public:
        Bureaucrat(std::string const name, int grade);
        ~Bureaucrat();
        Bureaucrat (const Bureaucrat& b);
        Bureaucrat& operator=(const Bureaucrat& b);

        std::string const   getName() const;
        int                 getGrade() const;
        void                incrementGrade();
        void                decrementGrade();
        void                checkGrade();
        void                signForm(Form& form);

        class GradeTooHighException : public std::exception 
        {
            const char * what () const throw ();
        };
        class GradeTooLowException : public std::exception
        {
            const char * what () const throw ();
        };

    private:
        std::string const   b_name;
        int                 b_grade;
};

std::ostream& operator<<(std::ostream& os, const Bureaucrat& b);

#endif