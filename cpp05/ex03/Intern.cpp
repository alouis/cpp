/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:42:33 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:42:33 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"

Intern::Intern() {}

Intern::~Intern() {}

Intern::Intern (const Intern& i)
{
    *this = i;
}

Intern& Intern::operator = (const Intern& i)
{
    (void)i;
    return *this;
}

Form*   createShrubberyForm(std::string const &target)
{
    ShrubberyCreationForm *new_form;
    new_form = new ShrubberyCreationForm(target);
    return new_form;
}

Form*   createRobotomyForm(std::string const &target)
{
    RobotomyRequestForm *new_form;
    new_form = new RobotomyRequestForm(target);
    return new_form;
}

Form*   createPresidentialForm(std::string const &target)
{
    PresidentialPardonForm *new_form;
    new_form = new PresidentialPardonForm(target);
    return new_form;
}

Form*   Intern::makeForm(std::string const &form_type, std::string const &target)
{
    std::string const forms[3] = {"shrubbery creation", "robotomy request", "presidential pardon"};
    Form*    (*creation_form[])(std::string const & target) = { 
        &createShrubberyForm, 
        &createRobotomyForm, 
        &createPresidentialForm, 
    };
    for (int i = 0; i < 3; i++)
    {
        if (form_type.compare(forms[i]) == 0)
        {
            std::cout << "Intern creates " << form_type << std::endl;
            return creation_form[i](target);
        }
    }
    std::cout << "This type of form doesn't exist" << std::endl;
    return 0;
}