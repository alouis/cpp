/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:42:50 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:42:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(std::string const name, int grade) : b_name(name), b_grade(grade)
{
    checkGrade();
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat::Bureaucrat (const Bureaucrat& b) : b_name(b.getName()), b_grade(b.getGrade())
{
    checkGrade();
}

Bureaucrat& Bureaucrat::operator = (const Bureaucrat& b)
{
    if (this != &b)
        this->b_grade = b.getGrade();
    checkGrade();
    return *this;
}

const char * Bureaucrat::GradeTooHighException::what () const throw ()
{
    return "Error : grade too high";
}

const char * Bureaucrat::GradeTooLowException::what () const throw ()
{
    return "Error : grade too low";
}


std::string const   Bureaucrat::getName() const
{
    return b_name;
}

int                 Bureaucrat::getGrade() const
{
    return b_grade;
}

void                Bureaucrat::incrementGrade()
{
    try
    {
        if (b_grade - 1 <= 0)
            throw GradeTooHighException();
        else
            b_grade -= 1;
    }
    catch (std::exception& exception) 
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Bureaucrat::decrementGrade()
{
    try
    {
        if (b_grade + 1 > 150)
            throw GradeTooLowException();
        else
            b_grade += 1;
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Bureaucrat::checkGrade()
{
    try 
    {
        if (b_grade < 1)
        {
            b_grade = 150;
            throw GradeTooHighException();
        }
        if (b_grade > 150)
        {
            b_grade = 150;
            throw GradeTooLowException();
        }
    }
    catch (std::exception& exception) 
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Bureaucrat::signForm(Form& form)
{
    if (b_grade > form.getGradeToSign())
        std::cout << b_name << " can't sign " << form.getName() << " because the grade required is higher" << std::endl;
    else
    {
        std::cout << b_name << " signs " << form.getName() << std::endl;
        form.bureaucratSigns(*this);
    }
}

void                Bureaucrat::executeForm (Form const & form)
{
    try
    {
        form.execute(*this);
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
        return ;
    }
    std::cout << b_name << " executs " << form.getName() << std::endl;
}

std::ostream&   operator<<(std::ostream& os, const Bureaucrat& b)
{
    os << b.getName() << ", bureaucrat grade " << b.getGrade() << std::endl;
    return os;
}