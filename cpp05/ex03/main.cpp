/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:42:36 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:42:37 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

int main()
{
    Bureaucrat young_bureaucrat("Ernest", 200);
    Bureaucrat old_bureaucrat("Allie", 1);

    std::cout << young_bureaucrat;
    std::cout << old_bureaucrat;
    old_bureaucrat.incrementGrade();
    std::cout << old_bureaucrat;
    young_bureaucrat.decrementGrade();
    std::cout << young_bureaucrat;
    old_bureaucrat.decrementGrade();
    std::cout << old_bureaucrat;
    young_bureaucrat.incrementGrade();
    std::cout << young_bureaucrat;

    ShrubberyCreationForm shrubForm("Forest");

    std::cout << shrubForm << std::endl;

    std::cout << "Novice bureaucrat tries executing ShrubberyCreationForm" << std::endl;
    try
    {
        shrubForm.execute(young_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        shrubForm.beSigned(young_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    
    std::cout << "Experienced bureaucrat tries executing ShrubberyCreationForm" << std::endl;
    try
    {
        shrubForm.execute(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        shrubForm.beSigned(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        shrubForm.execute(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }

    RobotomyRequestForm robotForm("a mouse");

    try
    {
        robotForm.beSigned(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        robotForm.execute(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
     
    PresidentialPardonForm presForm("Daffy Duck");

    try
    {
        presForm.beSigned(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }
    try
    {
        presForm.execute(old_bureaucrat);
    }
    catch (std::exception& error)
    {
        std::cerr << error.what() << std::endl;
    }

    young_bureaucrat.executeForm(presForm);
    old_bureaucrat.executeForm(presForm);

    Intern randomIntern;
    Form* rrf;
    Form* scf;
    Form* ppf;

    rrf = randomIntern.makeForm("robotomy request", "Bender");
    std::cout << std::endl << *rrf;

    scf = randomIntern.makeForm("shrubbery creation", "Bender");
    std::cout << std::endl << *scf;
    
    ppf = randomIntern.makeForm("non existing form", "Bender");

    ppf = randomIntern.makeForm("presidential pardon", "Bender");
    std::cout << std::endl << *ppf;

    old_bureaucrat.executeForm(*rrf);
    old_bureaucrat.signForm(*rrf);
    old_bureaucrat.executeForm(*rrf);

    return 0;
}