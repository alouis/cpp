/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:42:42 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:42:43 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(std::string const target) : Form("RobotomyRequestForm", 72, 45), r_target(target)
{
}

RobotomyRequestForm::~RobotomyRequestForm() {}

RobotomyRequestForm::RobotomyRequestForm (const RobotomyRequestForm& rrf) : Form("RobotomyRequestForm", 72, 45), r_target(rrf.r_target)
{
}

RobotomyRequestForm& RobotomyRequestForm::operator = (const RobotomyRequestForm& rrf)
{
    (void)rrf;
    return *this;
}

void    RobotomyRequestForm::execute(Bureaucrat const & executor) const
{
    Form::execute(executor);

    std::cout << "brrr brrrr psshhhh brrrrrr" << std::endl;
    std::cout << r_target << " was robotomized 50" << std::endl;
}

