/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:42:29 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:42:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

Form::Form(std::string name, int gradeToSign, int gradeToExecute) : f_name(name), f_signed(false), f_gradeToSign(gradeToSign), f_gradeToExecute(gradeToExecute)
{
    checkGradeToSign();
    checkGradeToExecute();
}

Form::~Form() {}

Form::Form (const Form& f) : f_name(f.getName()), f_signed(false), f_gradeToSign(f.getGradeToSign()), f_gradeToExecute(f.getGradeToExecute())
{
    checkGradeToSign();
    checkGradeToExecute();
}

Form& Form::operator = (const Form& f)
{
    if (this != &f)
    {
        this->f_signed = false;
        this->f_gradeToSign = f.getGradeToSign();
        this->f_gradeToExecute = f.getGradeToExecute();
    }
    checkGradeToSign();
    checkGradeToExecute();
    return *this;
}

const char * Form::GradeTooHighException::what () const throw ()
{
    return "Error: grade too high";
}

const char * Form::GradeTooLowException::what () const throw ()
{
    return "Error: grade too low";
}

const char * Form::FormNotSignedException::what () const throw ()
{
    return "Error: form not signed";
}

std::string const   Form::getName() const
{
    return f_name;
}

bool                Form::isSigned() const
{
    return f_signed;
}

int                 Form::getGradeToSign() const
{
    return f_gradeToSign;
}

int                 Form::getGradeToExecute() const
{
    return f_gradeToExecute;
}

void                Form::checkGradeToSign()
{
    try
    {
        if (f_gradeToSign < 1)
        {
            f_gradeToSign = 1;
            throw GradeTooHighException();
        }
        if (f_gradeToSign > 150)
        {
            f_gradeToSign = 150;
            throw GradeTooLowException();
        }
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Form::checkGradeToExecute()
{
    try
    {
        if (f_gradeToExecute < 1)
        {
            f_gradeToExecute = 1;
            throw GradeTooHighException();
        }
        if (f_gradeToExecute > 150)
        {
            f_gradeToExecute = 150;
            throw GradeTooLowException();
        }
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }
}

void                Form::beSigned(Bureaucrat& bureaucrat)
{
    if (bureaucrat.getGrade() > f_gradeToSign)
        throw GradeTooLowException();
    else
        f_signed = true;
    bureaucrat.signForm(*this);
}

void                Form::bureaucratSigns(Bureaucrat& bureaucrat)
{
    if (bureaucrat.getGrade() <= f_gradeToSign)
        f_signed = true;
}

void                Form::execute(Bureaucrat const & executor) const
{
    if (f_signed == false)
        throw FormNotSignedException();
    if (executor.getGrade() > f_gradeToExecute)
        throw GradeTooLowException();
}

std::ostream& operator<<(std::ostream& os, const Form& f)
{
    os << std::endl << "Form's name: " << f.getName() << std::endl;
    os << "Signed: ";
    if (f.isSigned())
        os << "yes" << std::endl;
    else
        os << "no" << std::endl;
    os << "Grade needed to be signed: " << f.getGradeToSign() << std::endl;
    os << "Grade needed to be executed: " << f.getGradeToExecute() << std::endl << std::endl;
    return os;
}