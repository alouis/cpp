/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:48 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:43:48 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string const target) : Form("PresidentialPardonForm", 72, 45), p_target(target)
{
}

PresidentialPardonForm::~PresidentialPardonForm() {}

PresidentialPardonForm::PresidentialPardonForm (const PresidentialPardonForm& ppf) : Form("PresidentialPardonForm", 72, 45), p_target(ppf.p_target)
{
}

PresidentialPardonForm& PresidentialPardonForm::operator = (const PresidentialPardonForm& ppf)
{
    (void)ppf;
    return *this;
}

void    PresidentialPardonForm::execute(Bureaucrat const & executor) const
{
    Form::execute(executor);

    std::cout << "Zafod Beeblebrox forgave " << p_target << std::endl;
}

