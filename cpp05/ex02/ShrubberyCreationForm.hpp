/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:41 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:43:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include <fstream>
# include "Form.hpp"

class ShrubberyCreationForm : public Form
{
    public:
        ShrubberyCreationForm(std::string const target);
        ~ShrubberyCreationForm();
        ShrubberyCreationForm (const ShrubberyCreationForm& scf);
        ShrubberyCreationForm& operator = (const ShrubberyCreationForm& scf);

        virtual void    execute (Bureaucrat const & executor) const;

    private:
        void            draw_trees(std::ofstream& file) const;
        
        class FileCreationException : public std::exception
        {
            const char * what () const throw ();
        };
        std::string const   s_target;
};

#endif