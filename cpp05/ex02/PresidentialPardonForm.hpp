/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:45 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:43:45 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP

# include "Form.hpp"

class PresidentialPardonForm : public Form
{
    public:
        PresidentialPardonForm(std::string const target);
        ~PresidentialPardonForm();
        PresidentialPardonForm (const PresidentialPardonForm& ppf);
        PresidentialPardonForm& operator = (const PresidentialPardonForm& ppf);

        virtual void    execute (Bureaucrat const & executor) const;

    private:
        std::string const   p_target; 
};

#endif