/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:49 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:43:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "Form.hpp"

class RobotomyRequestForm : public Form
{
    public:
        RobotomyRequestForm(std::string const target);
        ~RobotomyRequestForm();
        RobotomyRequestForm (const RobotomyRequestForm& rrf);
        RobotomyRequestForm& operator = (const RobotomyRequestForm& rrf);

        virtual void    execute (Bureaucrat const & executor) const;

    private:
        std::string const   r_target;
};

#endif