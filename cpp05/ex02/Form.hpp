/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:37 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:43:38 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include <string>
# include <ostream>
# include <exception>
# include "Bureaucrat.hpp"

class Bureaucrat;

class Form
{
    public:
        Form(std::string name, int gradeToSign, int gradeToExecute);
        ~Form();
        Form (const Form& f);
        Form& operator = (const Form& f);

        std::string const   getName() const;
        bool                isSigned() const;
        int                 getGradeToSign() const;
        int                 getGradeToExecute() const;
        void                beSigned(Bureaucrat& bureaucrat);
        void                bureaucratSigns(Bureaucrat& bureaucrat);
        virtual void        execute(Bureaucrat const & executor) const = 0;

        class GradeTooHighException : public std::exception
        {
            const char * what () const throw ();
        };
        class GradeTooLowException : public std::exception
        {
            const char * what () const throw ();
        };
        class FormNotSignedException : public std::exception
        {
            const char * what () const throw ();
        };

    private:
        void                checkGradeToSign();
        void                checkGradeToExecute();

        std::string const   f_name;
        bool                f_signed;
        int                 f_gradeToSign;
        int                 f_gradeToExecute;
};

std::ostream& operator<<(std::ostream& os, const Form& f);
#endif