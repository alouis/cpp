/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:43:51 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:45:47 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(std::string const target) : Form("ShrubberyCreationForm", 145, 137),  s_target(target)
{
}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

ShrubberyCreationForm::ShrubberyCreationForm (const ShrubberyCreationForm& scf) : Form("ShrubberyCreationForm", 145, 137),  s_target(scf.s_target)
{
}
        
ShrubberyCreationForm& ShrubberyCreationForm::operator = (const ShrubberyCreationForm& scf)
{
    (void)scf;
    return *this;
}

const char * ShrubberyCreationForm::FileCreationException::what () const throw ()
{
    return "Error when trying to create file";
}

void         ShrubberyCreationForm::execute(Bureaucrat const & executor) const
{
    Form::execute(executor);

    std::ofstream   file;
    std::string     name;
    
    name = s_target + "__shrubbery";
    const char *c_name = name.c_str();
    file.open(c_name);
    if (file)
        draw_trees(file);
    else
        throw FileCreationException();
}

void        ShrubberyCreationForm::draw_trees(std::ofstream& file) const
{
    file << "    *         *         *    " << std::endl;
    file << "   ***       ***       ***   " << std::endl;
    file << "   ***       ***       ***   " << std::endl;
    file << "  *****     *****     *****  " << std::endl;
    file << "  *****     *****     *****  " << std::endl;
    file << " *******   *******   ******* " << std::endl;
    file << "   | |       | |       | |   " << std::endl;
}