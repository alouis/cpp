/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:34:34 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:39:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

# include <iostream>
# include <exception>
# include <stdlib.h>
# include <vector>
# include <deque>
# include <list>
# include <array>

class NonExistingElement : public std::exception
{
    const char * what () const throw ()
    {
        return "easyfind(): couldn't find the element in the array";
    }
};

template <typename T>
typename T::iterator easyfind(T& container, int i)
{
    typename T::iterator it;

    for (it = container.begin(); it != container.end(); it++)
    {
        if (*it == i)
        {
            std::cout << std::distance(container.begin(), it);
            return it;
        }
    }
    throw NonExistingElement();
}

#endif