/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:34:31 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:39:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"

int main(void)
{
    int n;
    int i;

    std::cout << "----------- VECTOR -----------" << std::endl;
    std::vector<int>::iterator  iter_v;
    std::vector<int>            v(10);

    std::cout << "- Position of 3 in empty vector :" << std::endl;
    try
    {
        iter_v = ::easyfind(v, 3);
        std::cout << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    srand(time(NULL));
    for (i = 0; i < 10; i++)
    {
        n = rand() % 4;
        v.push_back(n);
    }

    std::cout << "- Position of 3 in vector with random numbers from 0 to 3" << std::endl;
    try
    {
        ::easyfind(v, 3);
        std::cout << std::endl;

    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }


    std::cout << std::endl << "----------- ARRAY -----------" << std::endl;
    std::array<int,5>           empty_array;
    std::array<int,6>           myArray = {0, 1, 2, 3, 4, 5};

    std::cout << "- Position of 3 in empty array :" << std::endl;
    try
    {
        ::easyfind(empty_array, 3);
        std::cout << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }

    std::cout << "- Position of 3 in array with numbers from 0 to 5" << std::endl;
    try
    {
        ::easyfind(myArray, 3);
        std::cout << std::endl;

    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }

    std::cout << std::endl << "----------- DEQUE -----------" << std::endl;
    int                 numbers0[] = {8, 29, -45, 324, 3};
    std::deque<int>     myDeque(numbers0, numbers0 + sizeof(numbers0) / sizeof(int));
    std::deque<int>  empty_deque;

    std::cout << "- Position of 3 in empty array :" << std::endl;
    try
    {
        ::easyfind(empty_deque, 3);
        std::cout << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    
    std::cout << "- Position of 3 in deque with random numbers" << std::endl;
    try
    {
        ::easyfind(myDeque, 3);
        std::cout << std::endl;

    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }

    std::cout << std::endl << "----------- LIST -----------" << std::endl;
    std::list<int>  empty_list;
    int             numbers1[] = {890, -67, 33, 0, 3, 56};
    std::list<int>  myList(numbers1, numbers1 + sizeof(numbers1) / sizeof(int));

    std::cout << "- Position of 3 in empty list :" << std::endl;
    try
    {
        ::easyfind(empty_list, 3);
        std::cout << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    
    std::cout << "- Position of 3 in list with random numbers" << std::endl;
    try
    {
        ::easyfind(myList, 3);
        std::cout << std::endl;

    }
    catch (std::exception &e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    
    return 0;
}