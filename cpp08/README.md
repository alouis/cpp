### Containers, iterator and algorithms

 * [Containers (FR)](https://docs.microsoft.com/fr-fr/cpp/standard-library/stl-containers?view=msvc-160)
 * [Template class examples](https://www.tutorialspoint.com/cplusplus/cpp_templates.htm)
 * [Man page iterators](https://www.cplusplus.com/reference/iterator/)
 * [Regular vs const iterator](https://www.geeksforgeeks.org/const-vs-regular-iterators-in-c-with-examples/)
 * [Why .ipp file instead of .cpp for template class](https://stackoverflow.com/questions/19147208/difference-between-using-ipp-extension-and-cpp-extension-files)
 * [std::stack doesn't expose iterators](https://stackoverflow.com/questions/525365/does-stdstack-expose-iterators)