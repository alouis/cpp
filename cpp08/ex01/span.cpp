/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:40:29 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:40:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"

Span::Span (unsigned int n) : _N(n)
{  
}

Span::~Span() 
{
}

Span::Span (const Span& s) : _N(s._N), _v(s._v)
{
}

Span& Span::operator=(const Span& s)
{
    if (this != &s)
    {
        this->_N = s._N;
        this->_v = s._v;
    }
    return *this;
}

void    Span::addNumber(unsigned int nbr)
{
    if (_v.size() < _N)
        _v.push_back(nbr);
    else
        throw TooManyNumbersException();
}


void    Span::addNumbers(unsigned int start, unsigned int end)
{
    if (end < start)
        throw InvalidRangeException();
    if (end - start > _N)
        throw TooManyNumbersException();
    while (start != end)
    {
        _v.push_back(start);
        start++;
    }
}

void    Span::showNumbers()
{
    std::vector<int>::iterator iter;

    for (iter = _v.begin(); iter != _v.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
}

int     Span::shortestSpan()
{
    std::vector<int>::iterator start;
    std::vector<int>::iterator next;

    if (_v.size() < 2)
        throw NoSpanException();
    start = _v.begin();
    next = _v.begin() + 1;
    long diff = std::abs(*next - *start);
    while (next != _v.end())
    {
        long new_diff = std::abs(*next - *start);
        if (new_diff < diff)
            diff = new_diff;
        next++;
        start++;
    }
    return diff;
}

int     Span::longestSpan()
{
    int min;
    int max;
    std::vector<int>::iterator it;

    if (_v.size() < 2)
        throw NoSpanException();
    it = _v.begin();
    min = *it;
    it = _v.begin() + 1;
    max = *it;
    if (min > max)
        ::swap(max, min);
    for (it = _v.begin() + 2; it != _v.end(); it++)
    {
        if (*it < min)
            min = *it;
        if (*it > max)
            max = *it;
    }
    return max - min;
}

const char* Span::TooManyNumbersException::what () const throw ()
{
    return "Too many numbers";
}

const char* Span::InvalidRangeException::what () const throw ()
{
    return "Invalid range";
}

const char* Span::NoSpanException::what () const throw ()
{
    return "Not enough numbers to calculate span";
}