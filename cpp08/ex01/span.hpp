/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:40:32 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:40:32 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP

# include <iostream>
# include <vector>
# include <complex>

template <typename T>
void    swap(T& a, T& b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

class Span
{
    public:
        Span(unsigned int n);
        ~Span();
        Span (const Span& s);
        Span& operator=(const Span& s);

        void    addNumber(unsigned int nbr);
        void    addNumbers(unsigned int start, unsigned int end);
        void    showNumbers();
        int     shortestSpan();
        int     longestSpan();

        class TooManyNumbersException : public std::exception
        {
            const char* what () const throw ();
        };

        class InvalidRangeException : public std::exception
        {
            const char* what () const throw ();
        };

        class NoSpanException : public std::exception
        {
            const char* what () const throw ();
        };

    private:
        Span();
        unsigned int        _N;
        std::vector<int>    _v;
};

#endif