/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:40:33 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:46:26 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"

int main()
{
    Span sp = Span(5);

    std::cout << "-------- Empty --------" << std::endl;
    try
    {
        sp.shortestSpan();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    try
    {
        sp.longestSpan();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
        
    std::cout << "-------- Subject's example --------" << std::endl;
    sp.addNumber(5);
    sp.addNumber(3);
    sp.addNumber(17);
    sp.addNumber(9);
    sp.addNumber(11);
    try
    {
        sp.addNumber(100);
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    sp.showNumbers();
    std::cout << "Shortest span: " << sp.shortestSpan() << std::endl;
    std::cout << "Longest span: " << sp.longestSpan() << std::endl;


    std::cout << "-------- Very big span (10000) --------" << std::endl;
    Span big_sp = Span(10000);
    try
    {
        big_sp.addNumbers(0, 10000);
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    
    std::cout << "Shortest span: " << big_sp.shortestSpan() << std::endl;
    std::cout << "Longest span: " << big_sp.longestSpan() << std::endl;

    return 0;
}