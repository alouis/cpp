/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 14:46:56 by alouis            #+#    #+#             */
/*   Updated: 2021/04/22 14:46:56 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MutantStack.hpp"

int main()
{
    MutantStack<int>    mstack;

    if (mstack.empty())
        std::cout << "Mutant stack is empty" << std::endl;
    
    for (int n = 0; n < 20; n++)
        mstack.push(n);
    std::cout << "Stack's size = " << mstack.size() << std::endl;

    MutantStack<int>::iterator  it;
    std::cout << "----- REGULAR iterator ------" << std::endl;
    for (it = mstack.begin(); it != mstack.end(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;

    MutantStack<int>::const_iterator  c_it;
    std::cout << "----- CONST iterator ------" << std::endl;
    for (c_it = mstack.begin(); c_it != mstack.end(); c_it++)
        std::cout << *c_it << " ";
    std::cout << std::endl;

    MutantStack<int>::reverse_iterator  r_it;
    std::cout << "----- REVERSE iterator ------" << std::endl;
    for (r_it = mstack.rbegin(); r_it != mstack.rend(); r_it++)
        std::cout << *r_it << " ";
    std::cout << std::endl;

    MutantStack<int>::const_reverse_iterator  cr_it;
    std::cout << "----- CONST REVERSE iterator ------" << std::endl;
    for (cr_it = mstack.rbegin(); cr_it != mstack.rend(); cr_it++)
        std::cout << *cr_it << " ";
    std::cout << std::endl;

    MutantStack<int> copy;

    copy = mstack;
    std::cout << "With copy of mutant stack" << std::endl;
    for (cr_it = copy.rbegin(); cr_it != copy.rend(); cr_it++)
        std::cout << *cr_it << " ";
    std::cout << std::endl;
    return 0;
}