/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:51:37 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:51:37 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(std::string n) : ClapTrap(n, 30, 20, 100, 100, 100, 100, 1, 5)
{
    std::cout << "FR4G-TP " << this->name << " has been created" << std::endl;
}

FragTrap::~FragTrap() 
{
    std::cout << "FR4G-TP " << this->name << " has been destroyed" << std::endl;
}

FragTrap::FragTrap (const FragTrap& f)
{
    *this = f;
}

FragTrap& FragTrap::operator = (const FragTrap& f)
{
    if (this != &f)
        ClapTrap::operator=(f);
    return *this;
}

int    FragTrap::rangedAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << this->name << " attacks " << target << ", causes damage " << this->ranged_attack_damage << std::endl;
    return 1;
}

int    FragTrap::meleeAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << this->name << " attacks " << target << ", causes damage " << this->melee_attack_damage << std::endl;
    return 1;
}


int    FragTrap::vaulthunter_dot_exe(std::string const & target)
{
    std::string attacks[] = {"chokes to death", "fires machine guns", "slices throat", "throws through window", "hunts down with an ax"};

    srand(time(NULL));
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    if (this->energy_points < 25)
    {
        std::cout << this->name << " doesn't have enough energy to throw a random attack, only " << this->energy_points << " left" << std::endl;
        return 0;
    }
    std::cout << this->name << " " << attacks[rand() % 5] << " , lost 25 hit points, causes 30 damages to " << target << std::endl;
    this->energy_points -= 25;
    return 1;
}