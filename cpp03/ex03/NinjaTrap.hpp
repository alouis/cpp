/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:51:50 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:51:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

# include <iostream>
# include <string>
# include <random>
# include "ClapTrap.hpp"
# include "ScavTrap.hpp"
# include "FragTrap.hpp"

class NinjaTrap : public ClapTrap
{
    public:
        NinjaTrap(std::string n);
        ~NinjaTrap();
        NinjaTrap (const NinjaTrap& nj);
        NinjaTrap &operator = (const NinjaTrap& nj);

        int     rangedAttack(std::string const & target);
        int     meleeAttack(std::string const & target);
        void    ninjaShoebox(ClapTrap &ennemy);
        void    ninjaShoebox(FragTrap &ennemy);
        void    ninjaShoebox(ScavTrap &ennemy);
        void    ninjaShoebox(NinjaTrap &ennemy);
};

#endif