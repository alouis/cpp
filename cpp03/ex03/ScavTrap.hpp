/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:51:54 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:51:55 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>
# include <string>
# include <stdlib.h>
# include <random>
# include "ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
    public:
        ScavTrap(std::string n);
        ~ScavTrap();
        ScavTrap(const ScavTrap& s);
        ScavTrap &operator = (const ScavTrap& s);
        
        int         rangedAttack(std::string const & target);
        int         meleeAttack(std::string const & target);
        int         challengeNewcomer(std::string const & target);
};

#endif