/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:51:46 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:51:47 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>
# include <string>
# include <stdlib.h>
# include <random>
# include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{
    public:
        FragTrap(std::string n);
        ~FragTrap();
        FragTrap (const FragTrap& f);
        FragTrap &operator = (const FragTrap& f);
        
        int         rangedAttack(std::string const & target);
        int         meleeAttack(std::string const & target);
        int         vaulthunter_dot_exe(std::string const & target);
};

#endif