/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:47:02 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:47:02 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>
# include <string>
# include <stdlib.h>

class FragTrap
{
    public:
        FragTrap(std::string n);
        ~FragTrap();
        FragTrap (const FragTrap& f);
        FragTrap &operator = (const FragTrap& f);

        std::string name;
        int         ranged_attack_damage;
        int         melee_attack_damage;
        
        int         rangedAttack(std::string const & target);
        int         meleeAttack(std::string const & target);
        void        takeDamage(unsigned int amount);
        void        beRepaired(unsigned int amount);
        int         vaulthunter_dot_exe(std::string const & target);

    private: 
        int         hit_points;
        int         max_hit_points;
        int         energy_points;
        int         max_energy_points;
        int         level;
        int         armor_damage_reduction;
};

#endif