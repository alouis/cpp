/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:46:59 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:47:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int main()
{
    FragTrap robot1("Nelson");
    FragTrap robot2("Hector");


    if (robot1.meleeAttack(robot2.name))
        robot2.takeDamage(robot2.melee_attack_damage);
    std::cout << std::endl;

    if (robot1.rangedAttack(robot2.name))
        robot2.takeDamage(robot2.melee_attack_damage);
    robot2.beRepaired(50);

    std::cout << std::endl;
    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);

    std::cout << std::endl;
    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);
    robot1.beRepaired(50);

    std::cout << std::endl;
    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);

    std::cout << std::endl;
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);

    std::cout << std::endl;
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);


    std::cout << std::endl;
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);

    std::cout << std::endl;
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);

    std::cout << std::endl;
    robot1.beRepaired(50);
}