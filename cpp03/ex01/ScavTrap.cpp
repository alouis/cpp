/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:47:59 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:47:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string n)
{
    hit_points = 100;
    max_hit_points = 100;
    energy_points = 50;
    max_energy_points = 50;
    level = 1;
    name = n;
    melee_attack_damage = 20;
    ranged_attack_damage = 15;
    armor_damage_reduction = 3;
    std::cout << "*** Manufacturing ScavTrap " << name << " ***" << std::endl;
}

ScavTrap::~ScavTrap()
{
    std::cout << "*** ScavTrap " << name << " now disappeared ***" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap& s)
{
    *this = s;
}

ScavTrap& ScavTrap::operator = (const ScavTrap& s)
{
    if (this != &s)
    {
        this->hit_points = s.hit_points;
        this->max_hit_points = s.max_hit_points;
        this->energy_points = s.energy_points;
        this->max_energy_points = s.max_energy_points;
        this->level = s.level;
        this->name = s.name;
        this->melee_attack_damage = s.melee_attack_damage;
        this->ranged_attack_damage = s.ranged_attack_damage;
        this->armor_damage_reduction = s.armor_damage_reduction;
        this->name = s.name;
    }
    return *this;
}

int    ScavTrap::rangedAttack(std::string const & target)
{
    if (hit_points <= 0)
    {
        std::cout << name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << name << " attacks " << target << ", causes damage " << ranged_attack_damage << std::endl;
    return 1;
}

int    ScavTrap::meleeAttack(std::string const & target)
{
    if (hit_points <= 0)
    {
        std::cout << name << " has been destroyed and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "*** ScavTrap " << name << " attacks " << target << ", causes damage " << melee_attack_damage << " ***" << std::endl;
    return 1;
}

void    ScavTrap::takeDamage(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " is already destroyed" << std::endl;
        return;
    }
    points_left = hit_points - amount - armor_damage_reduction;
    if (points_left <= 0)
    {
        hit_points = 0;
        std::cout << name << " just died horribly" << std::endl;
    }
    else
    {
        hit_points = points_left;
        std::cout << "*** " << name << " has " << hit_points << " left ***" << std::endl;
    }
}

void    ScavTrap::beRepaired(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " has already been destroyed and can't resuscitate" << std::endl;
        return;
    }
    points_left = hit_points + amount;
    if (points_left > max_hit_points)
        hit_points = max_hit_points;
    else
        hit_points = points_left;
    points_left = energy_points + amount;
    if (points_left > max_energy_points)
        energy_points = max_energy_points;
    else
        energy_points = points_left;
    std::cout << name << " was repaired and has now " << hit_points << " left tadaaaaam" << std::endl;
}

int     ScavTrap::challengeNewcomer(std::string const & target)
{
    std::string challenges[] = {"head or toe", "riddle", "puzzle", "tic-tac-toe", "draw lots"};
    std::string issue[] = {"loose", "win"};

    srand(time(NULL));
    if (hit_points <= 0)
    {
        std::cout << name << " has already been destroyed and can't attack anymore" << std::endl;
        return 0;
    }
    if (energy_points < 25)
    {
        std::cout << name << " doesn't have enough energy to throw a random attack, only " << energy_points << " left" << std::endl;
        return 0;
    }
    std::cout << "*** " << name << " gives " << target << " the following challenge : " << challenges[rand() % 5] << " and looses 25 hit points ***" << std::endl;
    energy_points -= 25;
    if (issue[rand() % 2] == "loose")
    {
        std::cout << target << " lost" << std::endl;
        return 1;
    }
    return 0;
}