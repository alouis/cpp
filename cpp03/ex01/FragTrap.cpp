/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:48:09 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:48:09 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(std::string n)
{
    hit_points = 100;
    max_hit_points = 100;
    energy_points = 100;
    max_energy_points = 100;
    level = 1;
    name = n;
    melee_attack_damage = 30;
    ranged_attack_damage = 20;
    armor_damage_reduction = 5;
    std::cout << "FR4G-TP " << name << " has been created" << std::endl;
}

FragTrap::~FragTrap() 
{
    std::cout << "FR4G-TP " << name << " has been destroyed" << std::endl;
}

FragTrap::FragTrap (const FragTrap& f)
{
    *this = f;
}

FragTrap& FragTrap::operator = (const FragTrap& f)
{
    if (this != &f)
    {
        this->hit_points = f.hit_points;
        this->max_hit_points = f.max_hit_points;
        this->energy_points = f.energy_points;
        this->max_energy_points = f.max_energy_points;
        this->level = f.level;
        this->name = f.name;
        this->melee_attack_damage = f.melee_attack_damage;
        this->ranged_attack_damage = f.ranged_attack_damage;
        this->armor_damage_reduction = f.armor_damage_reduction;
        this->name = f.name;
    }
    return *this;
}

int    FragTrap::rangedAttack(std::string const & target)
{
    if (hit_points <= 0)
    {
        std::cout << name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << name << " attacks " << target << ", causes damage " << ranged_attack_damage << std::endl;
    return 1;
}

int    FragTrap::meleeAttack(std::string const & target)
{
    if (hit_points <= 0)
    {
        std::cout << name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << name << " attacks " << target << ", causes damage " << melee_attack_damage << std::endl;
    return 1;
}

void    FragTrap::takeDamage(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " is already dead" << std::endl;
        return;
    }
    points_left = hit_points - amount - armor_damage_reduction;
    if (points_left <= 0)
    {
        hit_points = 0;
        std::cout << name << " just died" << std::endl;
    }
    else
    {
        hit_points = points_left;
        std::cout << name << " has " << hit_points << " left" << std::endl;
    }
}

void    FragTrap::beRepaired(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " is alredy dead and can't resuscitate" << std::endl;
        return;
    }
    points_left = hit_points + amount;
    if (points_left > max_hit_points)
        hit_points = max_hit_points;
    else
        hit_points = points_left;
    points_left = energy_points + amount;
    if (points_left > max_energy_points)
        energy_points = max_energy_points;
    else
        energy_points = points_left;
    std::cout << name << " was repaired and has now " << hit_points << " left" << std::endl;
}

int    FragTrap::vaulthunter_dot_exe(std::string const & target)
{
    std::string attacks[] = {"chokes to death", "fires machine guns", "slices throat", "throws through window", "hunts down with an ax"};

    srand(time(NULL));
    if (hit_points <= 0)
    {
        std::cout << name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    if (energy_points < 25)
    {
        std::cout << name << " doesn't have enough energy to throw a random attack, only " << energy_points << " left" << std::endl;
        return 0;
    }
    std::cout << name << " " << attacks[rand() % 5] << " , lost 25 hit points, causes 30 damages to " << target << std::endl;
    energy_points -= 25;
    return 1;
}