/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:47:56 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:47:57 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>
# include <string>
# include <stdlib.h>

class ScavTrap
{
    public:
        ScavTrap(std::string n);
        ~ScavTrap();
        ScavTrap(const ScavTrap& s);
        ScavTrap &operator = (const ScavTrap& s);

        std::string name;
        int         melee_attack_damage;
        int         ranged_attack_damage;

        int         rangedAttack(std::string const & target);
        int         meleeAttack(std::string const & target);
        void        takeDamage(unsigned int amount);
        void        beRepaired(unsigned int amount);
        int         challengeNewcomer(std::string const & target);

    private:
        int         hit_points;
        int         max_hit_points;
        int         energy_points;
        int         max_energy_points;
        int         level;
        int         armor_damage_reduction;
};

#endif