/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:48:55 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:48:55 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap() {}

ClapTrap::ClapTrap(std::string n, int ma, int ra, int h, int max_h, int e, int max_e, int l, int a) :
name(n),
melee_attack_damage(ma),
ranged_attack_damage(ra),
hit_points(h),
max_hit_points(max_h),
energy_points(e),
max_energy_points(max_e),
level(l),
armor_damage_reduction(a)
{
    std::cout << "ClapTrap ON ITS WAY to create a new ClapTrap named " << name << std::endl;
}

ClapTrap::~ClapTrap()
{
    std::cout << "The ClapTrap who created " << name << " is slowly fading away..." << std::endl;
}

ClapTrap::ClapTrap (const ClapTrap& c)
{
    *this = c;
}

ClapTrap&    ClapTrap::operator = (const ClapTrap& c)
{
    if (this != &c)
    {
        this->hit_points = c.hit_points;
        this->max_hit_points = c.max_hit_points;
        this->energy_points = c.energy_points;
        this->max_energy_points = c.max_energy_points;
        this->level = c.level;
        this->name = c.name;
        this->melee_attack_damage = c.melee_attack_damage;
        this->ranged_attack_damage = c.ranged_attack_damage;
        this->armor_damage_reduction = c.armor_damage_reduction;
        this->name = c.name;
    }
    return *this;
}

void ClapTrap::takeDamage(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " is already dead" << std::endl;
        return;
    }
    points_left = hit_points - amount - armor_damage_reduction;
    if (points_left <= 0)
    {
        hit_points = 0;
        std::cout << name << " just died" << std::endl;
    }
    else
    {
        hit_points = points_left;
        std::cout << name << " has " << hit_points << " hit points left" << std::endl;
    }
}

void ClapTrap::beRepaired(unsigned int amount)
{
    int points_left;

    if (hit_points <= 0)
    {
        std::cout << name << " is alredy dead and can't resuscitate" << std::endl;
        return;
    }
    points_left = hit_points + amount;
    if (points_left > max_hit_points)
        hit_points = max_hit_points;
    else
        hit_points = points_left;
    points_left = energy_points + amount;
    if (points_left > max_energy_points)
        energy_points = max_energy_points;
    else
        energy_points = points_left;
    std::cout << name << " was repaired and has now " << hit_points << " hit points left" << std::endl;
}
