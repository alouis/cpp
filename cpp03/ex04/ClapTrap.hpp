/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:52:33 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:52:34 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>
# include <string>

class ClapTrap
{
    public:
        ClapTrap();
        ClapTrap(std::string n, int ma, int ra, int h, int max_h, int e, int max_e, int l, int a);
        ~ClapTrap();
        ClapTrap (const ClapTrap& c);
        ClapTrap &operator = ( const ClapTrap& c);

        std::string name;
        int         melee_attack_damage;
        int         ranged_attack_damage;
        int         hit_points;
        
        void        takeDamage(unsigned int amount);
        void        beRepaired(unsigned int amount);
        
    protected:
        int         max_hit_points;
        int         energy_points;
        int         max_energy_points;
        int         level;
        int         armor_damage_reduction;
};

#endif