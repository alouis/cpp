/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:53:17 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:53:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP

# include <iostream>
# include <string>
# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class SuperTrap : public FragTrap, public NinjaTrap
{
    public:
        SuperTrap(std::string n);
        ~SuperTrap();
        SuperTrap (const SuperTrap& sp);
        SuperTrap& operator = (const SuperTrap& sp);

        int         rangedAttack(std::string const & target);
        int         meleeAttack(std::string const & target);
};

#endif