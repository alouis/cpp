/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:52:50 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:52:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"
#include "ClapTrap.hpp"

int main()
{
    FragTrap robot1("Nelson");
    FragTrap robot2("Hector");
    ScavTrap scavenger1("Douglas");
    ScavTrap scavenger2("Kelly");
    NinjaTrap ninja1("Travis");
    ClapTrap clap1("CLAP-CLAP-TRAP", 100, 100, 100, 100, 100, 100, 10, 0);
    SuperTrap   super1("Heroe-man");

    std::cout << std::endl;
    
    if (robot1.meleeAttack(robot2.name))
        robot2.takeDamage(robot2.melee_attack_damage);
    std::cout << std::endl;

    if (scavenger2.rangedAttack(scavenger1.name))
        scavenger1.takeDamage(scavenger1.ranged_attack_damage);
    std::cout << std::endl;

    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);
    std::cout << std::endl;

    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);
    robot1.beRepaired(50);
    std::cout << std::endl;

    if (robot2.vaulthunter_dot_exe(robot1.name))
        robot1.takeDamage(30);
    std::cout << std::endl;
    
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);
    std::cout << std::endl;
    
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);
    std::cout << std::endl;
    
    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);
    std::cout << std::endl;

    if (robot2.rangedAttack(robot1.name))
        robot1.takeDamage(robot1.ranged_attack_damage);
    std::cout << std::endl;
 
    if (scavenger1.rangedAttack(scavenger2.name))
        scavenger2.takeDamage(scavenger2.ranged_attack_damage);
    std::cout << std::endl;

    if (scavenger1.meleeAttack(scavenger2.name))
        scavenger2.takeDamage(scavenger2.melee_attack_damage);
    std::cout << std::endl;

    if (scavenger2.meleeAttack(robot2.name))
        robot2.takeDamage(scavenger2.melee_attack_damage);
    std::cout << std::endl;

    if (scavenger1.challengeNewcomer(robot2.name))
        robot2.takeDamage(20);
    std::cout << std::endl;

    if (scavenger2.challengeNewcomer(robot2.name))
        robot2.takeDamage(20);
    std::cout << std::endl;

    robot1.beRepaired(50);
    std::cout << std::endl;
    
    ninja1.ninjaShoebox(clap1);
    std::cout << std::endl;

    ninja1.ninjaShoebox(clap1);
    std::cout << std::endl;

    ninja1.ninjaShoebox(clap1);
    std::cout << std::endl;

    ninja1.ninjaShoebox(robot2);
    std::cout << std::endl;

    ninja1.ninjaShoebox(scavenger2);
    std::cout << std::endl;

    scavenger2.beRepaired(75);
    std::cout << std::endl;
    scavenger1.beRepaired(75);
    std::cout << std::endl;

    super1.ninjaShoebox(clap1);
    std::cout << std::endl;

    if (super1.rangedAttack(ninja1.name))
        ninja1.takeDamage(super1.ranged_attack_damage);
    std::cout << std::endl;

    if (super1.meleeAttack(scavenger1.name))
        scavenger1.takeDamage(super1.ranged_attack_damage);
    std::cout << std::endl;

    if (super1.vaulthunter_dot_exe(scavenger2.name))
        scavenger2.takeDamage(10);
    std::cout << std::endl;

    super1.ninjaShoebox(scavenger2);
    std::cout << std::endl;
    return 0;
}