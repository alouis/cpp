/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:53:11 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:53:11 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string n) : ClapTrap(n, 20, 15, 100, 100, 50, 50, 1, 3)
{
    std::cout << "*** Manufacturing ScavTrap " << this->name << " ***" << std::endl;
}

ScavTrap::~ScavTrap()
{
    std::cout << "*** ScavTrap " << this->name << " now disappeared ***" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap& s)
{
    *this = s;
}

ScavTrap& ScavTrap::operator = (const ScavTrap& s)
{
    if (this != &s)
        ClapTrap::operator=(s);
    return *this;
}

int    ScavTrap::rangedAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "FR4G-TP " << this->name << " attacks " << target << ", causes damage " << this->ranged_attack_damage << std::endl;
    return 1;
}

int    ScavTrap::meleeAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " has been destroyed and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "*** ScavTrap " << this->name << " attacks " << target << ", causes damage " << this->melee_attack_damage << " ***" << std::endl;
    return 1;
}

int     ScavTrap::challengeNewcomer(std::string const & target)
{
    std::string challenges[] = {"head or toe", "riddle", "puzzle", "tic-tac-toe", "draw lots"};
    std::string issue[] = {"loose", "win"};

    srand(time(NULL));
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " has already been destroyed and can't attack anymore" << std::endl;
        return 0;
    }
    if (this->energy_points < 25)
    {
        std::cout << this->name << " doesn't have enough energy to throw a random attack, only " << this->energy_points << " left" << std::endl;
        return 0;
    }
    std::cout << "*** " << this->name << " gives " << target << " the following challenge : " << challenges[rand() % 5] << " and looses 25 hit points ***" << std::endl;
    this->energy_points -= 25;
    if (issue[rand() % 2] == "loose")
    {
        std::cout << target << " lost" << std::endl;
        return 1;
    }
    return 0;
}