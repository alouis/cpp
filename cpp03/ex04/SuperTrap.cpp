/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:53:15 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:53:15 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"

SuperTrap::SuperTrap(std::string n) : 
                ClapTrap(n, 60 ,20, 100, 100, 120, 120, 1, 5),
                FragTrap(), NinjaTrap()
{
    std::cout << this->name << " suuuuuper trap all new to this world" << std::endl;
}

SuperTrap::~SuperTrap()
{
    std::cout << this->name << " suuuuuuuuper died" << std::endl;
}

SuperTrap::SuperTrap (const SuperTrap& sp)
{
    operator=(sp);
}

SuperTrap& SuperTrap::operator = (const SuperTrap& sp)
{
    if (this != &sp)
        ClapTrap::operator = (sp);
    return *this;
}

int SuperTrap::rangedAttack(std::string const & target)
{
    return FragTrap::rangedAttack(target);
}

int  SuperTrap::meleeAttack(std::string const & target)
{
    return NinjaTrap::meleeAttack(target);
}