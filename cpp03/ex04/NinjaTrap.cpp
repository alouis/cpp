/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/09 14:52:53 by alouis            #+#    #+#             */
/*   Updated: 2021/04/09 14:52:53 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap() : ClapTrap("Boris", 60, 5, 60, 60, 120, 120, 1, 0)
{
    std::cout << "Yihaaaaaaa, " << this->name << " just got a ninja license and a default name" << std::endl;
}

NinjaTrap::NinjaTrap(std::string n) : ClapTrap(n, 60, 5, 60, 60, 120, 120, 1, 0)
{
    std::cout << "Yihaaaaaaa, " << this->name << " just got a ninja license" << std::endl;
}

NinjaTrap::~NinjaTrap()
{
    std::cout << this->name << "'s ninja licensed revoked FOR EVER" << std::endl;
}

NinjaTrap::NinjaTrap (const NinjaTrap& nj)
{
    *this = nj;
}

NinjaTrap& NinjaTrap::operator = (const NinjaTrap& nj)
{
    if (this != &nj)
        ClapTrap::operator=(nj);
    return *this;
}

int    NinjaTrap::rangedAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "NINJAAAAA " << this->name << " attacks " << target << ", causes damage " << this->ranged_attack_damage << std::endl;
    return 1;
}

int    NinjaTrap::meleeAttack(std::string const & target)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return 0;
    }
    std::cout << "NINJAAAAA " << this->name << " attacks " << target << ", causes damage " << this->melee_attack_damage << std::endl;
    return 1;
}

void    NinjaTrap::ninjaShoebox(ClapTrap &ennemy)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return;
    }
    if (ennemy.hit_points <= 0)
    {
        std::cout << ennemy.name << " is dead and " << this->name << "'attack is useless" << std::endl;
        return;
    }
    std::cout << "Yihaaaaaa, " << this->name << " kicks " << ennemy.name << "'s bolts who looses 45 hit points" << std::endl;
    ennemy.takeDamage(45);
}

void    NinjaTrap::ninjaShoebox(FragTrap &ennemy)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return;
    }
    if (ennemy.hit_points <= 0)
    {
        std::cout << ennemy.name << " is dead and " << this->name << "'attack is useless" << std::endl;
        return;
    }
    std::cout << "Yihaaaaaa, " << this->name << " twists " << ennemy.name << "'s arms who looses 45 hit points" << std::endl;
    ennemy.takeDamage(45);
}

void    NinjaTrap::ninjaShoebox(ScavTrap &ennemy)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return;
    }
    if (ennemy.hit_points <= 0)
    {
        std::cout << ennemy.name << " is dead and " << this->name << "'attack is useless" << std::endl;
        return;
    }
    std::cout << "Yihaaaaaa, " << this->name << " throws rocks at " << ennemy.name << " who looses 45 hit points" << std::endl;
    ennemy.takeDamage(45);
}
        
void   NinjaTrap::ninjaShoebox(NinjaTrap &ennemy)
{
    if (this->hit_points <= 0)
    {
        std::cout << this->name << " is dead and can't attack anymore" << std::endl;
        return;
    }
    if (ennemy.hit_points <= 0)
    {
        std::cout << ennemy.name << " is dead and " << this->name << "'attack is useless" << std::endl;
        return;
    }
    std::cout << "Yihaaaaaa, " << this->name << " throws ultim ninja attack to teammate " << ennemy.name << " who dies" << std::endl;
    ennemy.takeDamage(1000);
}