/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:09:42 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:09:43 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "Human.hpp"

Human::Human(std::string chosen_name)
{
    name = chosen_name;
}

std::string    Human::identify()
{
    std::string         address;
    
    address = brain.identify();
    return address;
}

Brain   &Human::getBrain()
{
    return brain;
}