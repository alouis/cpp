/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:09:44 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:09:45 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
# define HUMAN_HPP

# include <iostream>
# include <string>
# include "Brain.hpp"

class Human
{
    public:
        Human(std::string chosen_name);
  
        std::string name;

        std::string  identify();
        Brain        &getBrain();        

    private:
        Brain       brain;
};

#endif