/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:09:38 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:09:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain() {}

std::string    Brain::identify()
{
    std::stringstream   strstream;
    std::string         address;

    strstream << this;
    address = strstream.str();
    return address;
}