/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:09:46 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:09:46 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include "Brain.hpp"

int main()
{
    Human person[] = {Human("Tommy"), Human("Polly"), Human("Finn")};
    
    for (int i=0; i<3; i++)
    {
        std::cout << person[i].name << "'s brain is there: " << std::endl;
        std::cout << person[i].identify() << std::endl;
        std::cout << person[i].getBrain().identify() << std::endl;
        std::cout << "There it is says the brain" << std::endl << std::endl;
    }

    return (0);
}