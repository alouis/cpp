/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:59:59 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:59:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie() {}

void    Zombie::setName(std::string name)
{
    z_name = name;
}

void    Zombie::setType(std::string type)
{
    z_type = type;
}

void    Zombie::announce()
{
    std::cout << z_name << " (" << z_type << ") " << "Braiiiiiiinnnssss ..." << std::endl;
}