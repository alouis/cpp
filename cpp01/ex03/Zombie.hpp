/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:00:01 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:00:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_H
# define ZOMBIE_H

# include <iostream>
# include <string>

class Zombie
{
    public:
        Zombie();

        void    announce();
        void    setName(std::string name);
        void    setType(std::string type);

    private:
        std::string z_name;
        std::string z_type;
};

#endif