/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:00:02 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:06:33 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n)
{
    int i;
    std::string random_names[] = {"Trevor", "Garçia", "Shilhaha", "Apük", "Troubadour", "I.K"};
    std::string random_types[] = {"Human", "Goldfish", "Moth", "Flamingo", "You", "Alien"};

    horde = new Zombie[n];
    srand(time(NULL));
    for (i=0; i<n; i++)
    {
        horde[i].setName(random_names[rand() % 6]);
        horde[i].setType(random_types[rand() % 6]);
        horde[i].announce();
    }
    nbrOfInstances = n;
}

ZombieHorde::~ZombieHorde()
{
    if (nbrOfInstances > 1)
        std::cout << "Destructing the horde of " << nbrOfInstances << " Zombies" << std::endl;
    else if (nbrOfInstances == 1)
        std::cout << "Destructing the horde of " << nbrOfInstances << " Zombie" << std::endl;
    else if (nbrOfInstances == 0)
        std::cout << "Destructing an empty horde" << std::endl;
    delete[] horde;
}