/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:59:56 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:06:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

int main()
{
    int number;

    std::cout << "---A horde of 5 zombies on its way...---" << std::endl;
    ZombieHorde Horde(5);
    number = Horde.nbrOfInstances;
    std::cout << "There are " << number << " zombies !" << std::endl;
    std::cout << std::endl;
    
    std::cout << "---Another horde of 10 zombies on its way...---" << std::endl;
    ZombieHorde BigHorde(10);
    number = BigHorde.nbrOfInstances;
    std::cout << "There are " << number << " zombies !" << std::endl;
    std::cout << std::endl;

    std::cout << "---Another horde of only 1 zombie on its way...---" << std::endl;
    ZombieHorde SmallHorde(1);
    number = SmallHorde.nbrOfInstances;
    std::cout << "There is " << number << " zombies !" << std::endl;
    std::cout << std::endl;

    std::cout << "---Not a horde really...---" << std::endl;
    ZombieHorde NoHorde(0);
    number = NoHorde.nbrOfInstances;
    std::cout << "There is " << number << " zombie !" << std::endl;
    std::cout << std::endl;

    return (0);
}