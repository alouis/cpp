/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:00:04 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:01:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEHORDE_H
# define ZOMBIEHORDE_H

# include <iostream>
# include <string>
# include <stdlib.h>
# include "Zombie.hpp"

class ZombieHorde
{
    public:
        ZombieHorde(int n);
        ~ZombieHorde();
        
        int     nbrOfInstances;

    private:
        Zombie  *horde;
        
        void    announceHorde();
};

#endif