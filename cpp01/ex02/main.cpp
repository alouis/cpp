/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:52:03 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:52:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

int main()
{
    std::cout << "Creating a Zombie on the stack" << std::endl;
    Zombie onTheStack("Rocky", "goldfish");
    std::cout << std::endl;

    std::cout << "Creating a Zombie on the heap" << std::endl;
    Zombie *onTheHeap;
    ZombieEvent zombieEvent;
    zombieEvent.setZombieType("human");
    onTheHeap = zombieEvent.newZombie("Paul");
    std::cout << std::endl;

    std::cout << "Creating a random Zombie on the heap" << std::endl;
    Zombie *randomZombie;
    zombieEvent.setZombieType("undetermined");
    randomZombie = zombieEvent.randomChump();
    std::cout << std::endl;

    onTheHeap->announce();
    std::cout << "AND ";
    randomZombie->announce();
    std::cout << "ARE DYING FOR GOOD" << std::endl;
    delete onTheHeap;
    delete randomZombie;
    onTheStack.announce();
    std::cout << "REMAINS" << std::endl;
    return (0);
}