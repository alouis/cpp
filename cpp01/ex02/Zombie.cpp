/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:52:06 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:52:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(std::string default_name, std::string default_type)
{
    z_name = default_name;
    z_type = default_type;
    announce();
}

void    Zombie::announce()
{
    std::cout << z_name << " (" << z_type << ") " << "Braiiiiiiinnnssss ..." << std::endl;
}