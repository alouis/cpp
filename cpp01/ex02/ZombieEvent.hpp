/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:52:11 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:59:08 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_H
# define ZOMBIEEVENT_H

# include <iostream>
# include <string>
# include <stdlib.h>
# include "Zombie.hpp"

class ZombieEvent
{
    public:
        ZombieEvent();

		void	setZombieType(std::string type);
        Zombie * newZombie (std::string name);
        Zombie * randomChump();

    private:
		std::string z_type;
};

#endif