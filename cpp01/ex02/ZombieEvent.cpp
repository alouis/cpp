/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:52:10 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:55:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent() {}

void    ZombieEvent::setZombieType(std::string type)
{
    z_type = type;
}

Zombie *ZombieEvent::newZombie (std::string new_name)
{
    Zombie  *newZombie;

    newZombie = new Zombie(new_name, z_type);
    
    return newZombie;
}

Zombie *ZombieEvent::randomChump ()
{
    Zombie      *newZombie;
    std::string name;
    std::string random_names[] = {"Trevor", "Garçia", "Shilhaha", "Apük", "Troubadour", "I.K"};

    srand(time(NULL));
    name = random_names[rand() % 6];
    newZombie = new Zombie(name, z_type);
    
    return newZombie;
}