/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:34:51 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:34:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fstream>
#include <string>
#include <iostream>

int main(int argc, char **argv)
{
    std::string     line;
    std::ifstream   file;

    if (argc == 1)
       while (1);
    for (int i=1; argv[i]; i++)
    {
        file.open(argv[i]);
        if (file)
        {
            while (std::getline(file, line))
            {
                std::cout << line << std::endl;
                line.clear();
            }
            file.close();
        }
        else
            std::cout << argv[i] << " no such file or directory" << std::endl;
    }
    return 0;
}