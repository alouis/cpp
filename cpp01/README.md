# Pointer and reference

### Definitions

* Pointers: A pointer is a variable that holds memory address of another variable. A pointer needs to be dereferenced with * operator to access the memory location it points to. 

* References : A reference variable is an alias, that is, another name for an already existing variable. A reference, like a pointer, is also implemented by storing the address of an object. 
A reference can be thought of as a constant pointer (not to be confused with a pointer to a constant value!) with automatic indirection, i.e the compiler will apply the * operator for you.

```
int i = 3; 

// pointer
int *ptr = &i; 

// reference
int &ref = i; 

```

### Useful links
 * [Distinguish between pointers and references in C++](http://www.cplusplus.com/articles/ENywvCM9/)
 * [When do we pass arguments by reference or pointer ?](https://www.geeksforgeeks.org/when-do-we-pass-arguments-by-reference-or-pointer/)

## Pointer to class and stringstream (ex05)
 * [Pointers to classes](https://www.cplusplus.com/doc/tutorial/classes/)
 * [Stringstream class](https://www.cplusplus.com/reference/sstream/stringstream/)
 * [Pointer 'this'](https://www.geeksforgeeks.org/this-pointer-in-c/)

## Pointeurs et références 
 * [Explications (FR)](https://developpement-informatique.com/article/303/fonctions-membres-en-c++)
 
## Tableaux de pointeurs sur fonctions
 * [Déclarer un tableau de pointeurs vers des fonctions](https://docs.microsoft.com/fr-fr/troubleshoot/cpp/declare-pointers-to-functions)
 * [Wikiversité: Pointeur, Tableaux et références](https://fr.wikiversity.org/wiki/Langage_C%2B%2B/Pointeur,_Tableaux_et_références)

## Handling exceptions (ex08)
 * [try, throw, and catch Statements](https://docs.microsoft.com/en-us/cpp/cpp/try-throw-and-catch-statements-cpp?view=msvc-160)
 * [C++ Exception Handling](https://www.guru99.com/cpp-exceptions-handling.html)