/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:25:51 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:32:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOGGER_HPP
# define LOGGER_HPP

# include <iostream>
# include <string>
# include <fstream>
# include <ctime>

class Logger
{
    public:
        Logger();

        void            log(std::string const &dest, std::string const &message);

    private:
        //char            *fileName;
        std::ofstream   file;

        void            logToFile(std::string const &message);
        void            logToConsole(std::string const &message);
        std::string     makeLogEntry(std::string const &message);
};

#endif