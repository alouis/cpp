/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:25:53 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:34:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"

Logger::Logger() {}

std::string Logger::makeLogEntry(std::string const &message)
{
    std::string log;
    std::string date;
    time_t      now = time(0);

    log = message;
    date = ctime(&now);
    date.append(log);
    
    return date;
}

void        Logger::logToFile(std::string const &message)
{
    std::string log;

    file.open("file.logs");
    log = makeLogEntry(message);
    file << log << std::endl;
    file.close();
}
       
void        Logger::logToConsole(std::string const &message)
{
    std::string log;

    log = makeLogEntry(message);
    std::cout << log << std::endl;
}

void        Logger::log(std::string const &dest, std::string const &message)
{
    int action;
    void    (Logger::* method[2])(std::string const &message) = {
        &Logger::logToFile,
        &Logger::logToConsole,
    };

    try
    {
        action = ("logToFile" == dest) ? 0:
            ("logToConsole" == dest) ? 1:
            throw "Not a referenced Logger's method";
    }
    catch (char const *exception)
    {
        std::cout << exception << std::endl;
        return ;
    }
    (this->*method[action])(message);
}