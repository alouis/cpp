/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:25:49 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:34:21 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"

int main()
{
    Logger logFile;

    logFile.log("logToConsole", "loooooog in CONSOLE");
    logFile.log("logToFile", "loooooog in FILE");
    
    return (0);
}