/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:23:51 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:25:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

int main()
{
    Human soldier;

    soldier.action("meleeAttack", "the ennemies");
    soldier.action("rangedAttack", "the ennemies' catapults");
    soldier.action("intimidatingShout", "the ennemies' dogs");
    soldier.action("bigHug", "the ennemies' moms");

    return (0);
}