/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:23:46 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:24:43 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

void    Human::meleeAttack(std::string const & target)
{
    std::cout << "meleeAttack on " << target << std::endl;
}

void    Human::rangedAttack(std::string const & target)
{
    std::cout << "rangedAttack on " << target << std::endl;
}

void    Human::intimidatingShout(std::string const & target)
{
    std::cout << "intimidatingShout at " << target << std::endl;
}

void    Human::action(std::string const & action_name, std::string const & target)
{
    int     action;
    void    (Human::* methods[3])(std::string const & target) = { 
        &Human::meleeAttack, 
        &Human::rangedAttack, 
        &Human::intimidatingShout 
    };

    try
    {
        action = ("meleeAttack" == action_name) ? 0 :
            ("rangedAttack" == action_name) ? 1 :
            ("intimidatingShout" == action_name) ? 2:
            throw "Attack not referenced";
    }
    catch (char const *exception)
    {
        std::cout << exception << std::endl;
        return ;
    }

	(this->*methods[action])(target);
}
