/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:18:59 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:18:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fstream>
#include <iostream>
#include <string>

bool    check_args(int argc, char **argv)
{
    std::string     s1;
    std::string     s2;
    std::string     filename;

    if (argc != 4)
    {
        std::cout << "Missing argument(s)" << std::endl;
        return false;
    }
    filename = argv[1];
    s1 = argv[2];
    s2 = argv[3];
    if (s1.compare(s2) == 0)
    {
        std::cout << "Word to be replaced is the same than word to replace with" << std::endl;
        return false;
    }
    if (!filename.size() || !s1.size() || !s2.size())
    {
        std::cout << "Empty argument(s) won't work" << std::endl;
        return false;
    }
    return true;
}

bool    replace(std::ifstream &file, std::string s1, std::string s2)
{
    std::string     line;
    std::size_t     found;
    std::ofstream   newFile;

    newFile.open("./FILENAME.replace");
    if (newFile)
    {
        while (std::getline(file, line))
        {
            found = 0;
            while ((found = line.find(s1, found)))
            {
                if (found != std::string::npos)
                    line.replace(found, s1.size(), s2);
                else
                    break ;
            }
            newFile << line << std::endl;
            line.clear();
        }
        newFile.close();
    }
    else
    {
        std::cout << "Error: FILENAME.replace couldn't be created" << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char **argv)
{
    std::ifstream   file;

    if (!(check_args(argc, argv)))
        return -1;
    file.open(argv[1]);
    if (file)
    {
        if (!replace(file, argv[2], argv[3]))
        {
            file.close();
            return -1;
        }
        file.close();
    }
    else
    {
        std::cout << "Error: can't open file" << std::endl;
        return -1;
    }
    return 0;
}