/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:07:33 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:08:54 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <iostream>
# include <string>

int main()
{
    std::string str;
    std::string *ptr;
    std::string &ref = str;

    str = "HI THIS IS BRAIN";
    ptr = &str;

    std::cout << "Pointer " << " = " << *ptr << std::endl;
    std::cout << "Reference " << " = " << ref << std::endl;

    return (0);
}