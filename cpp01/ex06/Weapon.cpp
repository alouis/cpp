/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:13:20 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:13:20 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon(std::string type) 
{
    setType(type);
}

void    Weapon::setType(std::string type)
{
    weapon = type;
}

const std::string   &Weapon::getType()
{
    const std::string &ref = weapon;

    return ref;
}