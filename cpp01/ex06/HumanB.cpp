/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:13:24 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:13:25 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(std::string name) : humanB(name), weaponB(0)
{
}

void    HumanB::setWeapon(Weapon &newWeapon)
{
    weaponB = &newWeapon;
}

void    HumanB::attack()
{
    std::string type;

    type = weaponB->getType();
    std::cout << humanB << " attacks with her " << type << std::endl;
}