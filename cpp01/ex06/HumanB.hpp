/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:13:22 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:13:23 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP

# include <string>
# include <iostream>
# include "Weapon.hpp"

class HumanB
{
    public:
        HumanB(std::string name);

        void        setWeapon(Weapon &newWeapon);
        void        attack();

    private:
        std::string humanB;
        Weapon      *weaponB;
};

#endif