/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:13:28 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:13:29 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon &newWeapon) : humanA(name), weaponA(newWeapon)
{
}

void    HumanA::attack()
{
    std::string type;

    type = weaponA.getType();
    std::cout << humanA << " attacks with his " << type << std::endl;
}