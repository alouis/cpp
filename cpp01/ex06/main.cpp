/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 16:13:07 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 16:17:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"
#include "HumanA.hpp"
#include "HumanB.hpp"

int main()
{
    Weapon club("crude spiked club");
    Weapon dagger("dagger");
    
    HumanA bob("Bob", club);
    bob.attack();
    club.setType("other type of club");
    bob.attack();

    HumanB janett("Janett");
    janett.setWeapon(dagger);
    janett.attack();
    dagger.setType("sword");
    janett.attack();

    return (0);
}