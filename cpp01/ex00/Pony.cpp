/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:44:26 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:44:27 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

int Pony::NbrOfInstances = 0;

Pony::Pony(std::string new_name, int new_height)
{
    name = new_name;
    height = new_height;
    NbrOfInstances++;
}

Pony::~Pony() {
    NbrOfInstances--;
}

void    Pony::displayInfos()
{
    std::cout << "This horse's name is " << name << " and he's " << height << " hands tall" << std::endl;
}

void    Pony::grazing()
{
    std::cout << name << " is grazing in the poppy field" << std::endl;
}

void    Pony::napping()
{
    std::cout << name << " is napping in the shadow of the poplar tree" << std::endl;
}

void    Pony::HowManyInstances()
{
    if (NbrOfInstances == 0)
        std::cout << "There's no horse" << std::endl;
    else if (NbrOfInstances == 1)
        std::cout << "There's one horse" << std::endl;
    else
        std::cout << "There are " << NbrOfInstances << " horses" << std::endl;
}