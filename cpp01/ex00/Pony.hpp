/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:44:37 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:44:37 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
# define PONY_HPP

# include <iostream>
# include <string>

class Pony
{
    public:
        Pony(std::string str, int n);
        ~Pony();

        std::string name;
        
        static void HowManyInstances();
        void        displayInfos();
        void        grazing();
        void        napping();
        
    private:
        int         height;
        static int  NbrOfInstances;
};

#endif