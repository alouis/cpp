/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 15:44:14 by alouis            #+#    #+#             */
/*   Updated: 2021/04/07 15:44:18 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void    ponyOnTheStack()
{
    Pony s_pony("BoJack", 17);

    std::cout << "---Pony On The Stack---" << std::endl;
    Pony::HowManyInstances();
    s_pony.displayInfos();
    s_pony.grazing();
    s_pony.napping();
    std::cout << "Leaving " << s_pony.name << std::endl;
    std::cout << std::endl;
}

void    ponyOnTheHeap()
{
    Pony *h_pony = new Pony("Spirit", 14);

    std::cout << "---Pony On The Heap---" << std::endl;
    Pony::HowManyInstances();
    h_pony->displayInfos();
    h_pony->grazing();
    h_pony->napping();
    std::cout << "Leaving " << h_pony->name << std::endl;
    std::cout << std::endl;
    delete h_pony;
}

int main()
{
    std::cout << "---Begining of the program---" << std::endl;
    Pony::HowManyInstances();
    std::cout << std::endl;
    ponyOnTheStack();
    Pony::HowManyInstances();
    std::cout << std::endl;
    ponyOnTheHeap();
    std::cout << "---End of the program---" << std::endl;
    Pony::HowManyInstances();
    std::cout << std::endl;
}