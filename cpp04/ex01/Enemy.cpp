/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:09:16 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:09:16 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy() {}

Enemy::Enemy(int start_hp, std::string const & chosen_type) : hp(start_hp), type(chosen_type)
{}

Enemy::~Enemy() {}

Enemy::Enemy (const Enemy& e)
{
    *this = e;
}

Enemy& Enemy::operator = (const Enemy& e)
{
    if (this != &e)
    {
        this->hp = e.getHP();
        this->type = e.getType();
    }
    return *this;
}

std::string Enemy::getType() const
{
    return type;
}

int         Enemy::getHP() const
{
    return hp;
}
