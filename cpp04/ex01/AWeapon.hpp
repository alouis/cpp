/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:08:39 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:08:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
# define AWEAPON_HPP

# include <iostream>
# include <string>

class AWeapon
{
    public:
        AWeapon(std::string const & n, int apc, int d);
        virtual ~AWeapon();
        AWeapon (const AWeapon& aw);
        AWeapon& operator = (const AWeapon& aw);

        std::string     getName() const;
        int             getAPCost() const;
        int             getDamage() const;
        virtual void    attack() const = 0;

    protected:
        AWeapon();
        std::string     name;
        int             apcost;
        int             damage;
};

#endif