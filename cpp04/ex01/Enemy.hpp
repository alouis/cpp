/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:09:21 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:09:21 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
# define ENEMY_HPP

# include <iostream>
# include <string>

class Enemy 
{
    public:
        Enemy(int hp, std::string const & type);
        virtual ~Enemy();
        Enemy (const Enemy& e);
        Enemy& operator = (const Enemy& e);

        std::string         getType() const;
        int                 getHP() const;
        virtual void        takeDamage(int) = 0;

    protected:
        Enemy();
        int         hp;
        std::string type;
};

#endif