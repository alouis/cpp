/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:08:05 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:08:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon() {}

AWeapon::AWeapon(std::string const & n, int apc, int d) : 
name(n),
apcost(apc),
damage(d)
{}

AWeapon::~AWeapon() {}

AWeapon::AWeapon (const AWeapon& aw)
{
    *this = aw;
}

AWeapon& AWeapon::operator = (const AWeapon& aw)
{
    if (this != &aw)
    {
        this->name = aw.getName();
        this->apcost = aw.getAPCost();
        this->damage = aw.getDamage();
    }
    return *this;
}

std::string AWeapon::getName() const
{
    return name;
}

int         AWeapon::getAPCost() const
{
    return apcost;
}

int         AWeapon::getDamage() const
{
    return damage;
}
