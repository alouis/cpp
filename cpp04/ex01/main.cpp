/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:10:06 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:10:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "SuperMutant.hpp"
#include "RadScorpion.hpp"
#include "Character.hpp"

int main()
{
    PlasmaRifle weapon1;
    PowerFist   weapon2;
    SuperMutant*     supermutant1 = new SuperMutant();
    RadScorpion*     radscorpion1 = new RadScorpion();
    Character* character1 = new Character("Mercredi");

    std::cout << *character1 << std::endl;
    character1->equip(&weapon1);
    std::cout << *character1 << std::endl;
    std::cout << "Enemy rad scorpion has "<< radscorpion1->getHP() << " hp" << std::endl;
    character1->attack(radscorpion1);
    std::cout << "Enemy rad scorpion has "<< radscorpion1->getHP() << " hp" << std::endl;
    std::cout << *character1 << std::endl;
    character1->recoverAP();
    character1->recoverAP();
    character1->recoverAP();
    character1->recoverAP();
    character1->equip(&weapon2);
    std::cout << *character1 << std::endl;
    std::cout << "Enemy rad scorpion has "<< radscorpion1->getHP() << " hp" << std::endl;
    character1->attack(radscorpion1);
    std::cout << "Enemy rad scorpion has "<< radscorpion1->getHP() << " hp" << std::endl;
    std::cout << *character1 << std::endl;
    
    character1->recoverAP();
    std::cout << *character1 << std::endl;
    character1->equip(&weapon1);
    std::cout << *character1 << std::endl;
    std::cout << "Enemy super mutant has "<< supermutant1->getHP() << " hp" << std::endl;
    character1->attack(supermutant1);
    std::cout << "Enemy super mutant has "<< supermutant1->getHP() << " hp" << std::endl;
    std::cout << *character1 << std::endl;
    character1->recoverAP();
    character1->recoverAP();
    character1->recoverAP();
    character1->recoverAP();
    character1->equip(&weapon2);
    std::cout << *character1 << std::endl;
    std::cout << "Enemy super mutant has "<< supermutant1->getHP() << " hp" << std::endl;
    character1->attack(supermutant1);
    std::cout << "Enemy super mutant has "<< supermutant1->getHP() << " hp" << std::endl;
    std::cout << *character1 << std::endl;

    character1->attack(radscorpion1);
    delete character1;
    delete supermutant1;
    return 0;
}