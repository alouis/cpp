/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:09:08 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:09:09 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.hpp"

RadScorpion::RadScorpion() : Enemy(80, "Rad Scorpion")
{
    std::cout << "* click click click *" << std::endl;
}

RadScorpion::~RadScorpion()
{
    std::cout << "* SPROTCH *" << std::endl;
}

RadScorpion::RadScorpion (const RadScorpion& rs)
{
    *this = rs;
}

RadScorpion& RadScorpion::operator = (const RadScorpion& rs)
{
    if (this != &rs)
    {
        this->hp = rs.getHP();
        this->type = rs.getType();
    }
    return *this;
}

void    RadScorpion::takeDamage(int hits)
{
    if (hits > 0)
        hp = hp - hits;
}