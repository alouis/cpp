/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:10:20 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:14:48 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

SuperMutant::SuperMutant() : Enemy(170, "Super Mutant")
{
    std::cout << "Gaaah. Me want smash heads!!" << std::endl;
}

SuperMutant::~SuperMutant()
{
    std::cout << "Aaargh ..." << std::endl;
}

SuperMutant::SuperMutant (const SuperMutant& sm)
{
    *this = sm;
}

SuperMutant& SuperMutant::operator = (const SuperMutant& sm)
{
    if (this != &sm)
    {
        this->hp = sm.getHP();
        this->type = sm.getType();
    }
    return *this;
}

void    SuperMutant::takeDamage(int hits)
{
    hits = hits - 3;
    if (hits > 0)
        hp = hp - hits;
}