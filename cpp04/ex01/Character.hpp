/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:08:49 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:08:56 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include <iostream>
# include <string>
# include <ostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class Character
{
    public:
        Character(std::string const & name);
        ~Character();
        Character (const Character& c);
        Character& operator = (const Character& c);
                
                
        void recoverAP();
        void equip(AWeapon *_weapon);
        void attack(Enemy *enemy);

        std::string getName() const;
        int         getAP() const;
        std::string weaponName() const;
        AWeapon      *getWeapon() const;

    private:
        std::string name;
        int         AP;
        AWeapon     *weapon;

};

std::ostream& operator<<(std::ostream& os, const Character& c);

#endif