/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:10:11 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:10:12 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50) {}

PowerFist::~PowerFist() {}

PowerFist::PowerFist (const PowerFist& pf)
{
    *this = pf;
}

PowerFist& PowerFist::operator = (const PowerFist& pf)
{
    if (this != &pf)
    {
        this->name = pf.getName();
        this->apcost = pf.getAPCost();
        this->damage = pf.getDamage();
    }
    return *this;
}

void    PowerFist::attack() const
{
    std::cout << "* pschhh... SBAM ! *" << std::endl;
}