/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:08:41 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:16:12 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(std::string const & chosen_name) : name(chosen_name), AP(40) //weapon(0)
{}

Character::~Character() {}

Character::Character (const Character& c)
{
    *this = c;
}

Character& Character::operator = (const Character& c)
{
    if (this != &c)
        this->name = c.getName();
    return *this;
}

std::string Character::getName() const
{
    return name;
}

int         Character::getAP() const
{
    return AP;
}

void        Character::recoverAP()
{
    if (AP + 10 < 40)
        AP = AP + 10;
    else
        AP = 40;
}

void        Character::equip(AWeapon *_weapon)
{
    this->weapon = _weapon;
}


std::string Character::weaponName() const
{
    return this->weapon->getName();
}

AWeapon     *Character::getWeapon() const
{
    return this->weapon;
}

void        Character::attack(Enemy* enemy)
{
    if (AP <= 0 || AP - weapon->getAPCost() < 0)
    {
        std::cout << name << " doesn't have enough AP left, can't attack " << enemy->getType() << std::endl;
        return ;
    }
    if (weapon == 0)
    {
        std::cout << name << " doesn't have an arm, can't attack " << enemy->getType() << std::endl;
        return ;
    }
    std::cout << name << " attacks " << enemy->getType() << " with a " << weaponName() << std::endl;
    weapon->attack();
    enemy->takeDamage(weapon->getDamage());
    AP = AP - weapon->getAPCost();
    if (enemy->getHP() <= 0)
    {
        delete enemy;
        enemy = NULL;
    }
}

std::ostream& operator<<(std::ostream& os, const Character& c)
{
    if (c.getWeapon() == 0)
        os << c.getName() << " has " << c.getAP() << " AP and is unarmed" << std::endl;
    else
        os << c.getName() << " has " << c.getAP() << " AP and wields a " << c.weaponName() << std::endl;
    return os;
}