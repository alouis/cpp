/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:09:05 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:09:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RADSCORPION_HPP
# define RADSCORPION_HPP

# include <iostream>
# include <string>
# include "Enemy.hpp"

class RadScorpion : public Enemy
{
    public:
        RadScorpion();
        ~RadScorpion();
        RadScorpion (const RadScorpion& rs);
        RadScorpion& operator = (const RadScorpion& rs);

        virtual void    takeDamage(int hits);
};

#endif