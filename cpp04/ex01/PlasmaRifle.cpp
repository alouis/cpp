/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:09:13 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:09:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle() :
    AWeapon("Plasma Rifle", 5, 21)
{}

PlasmaRifle::~PlasmaRifle() {}
    
PlasmaRifle::PlasmaRifle (const PlasmaRifle& pr)
{
    *this = pr;
}

PlasmaRifle& PlasmaRifle::operator = (const PlasmaRifle& pr)
{
    if (this != &pr)
    {
        this->name = pr.getName();
        this->apcost = pr.getAPCost();
        this->damage = pr.getDamage();
    }
    return *this;
}

void        PlasmaRifle::attack() const
{
    std::cout << "* piouuu piouuu piouuu *" << std::endl;
}