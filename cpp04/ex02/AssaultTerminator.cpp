/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:17:05 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:17:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"

AssaultTerminator::AssaultTerminator()
{
    std::cout << "* teleports from space *" << std::endl;
}

AssaultTerminator::~AssaultTerminator()
{
    std::cout << "I’ll be back ..." << std::endl;
}

AssaultTerminator::AssaultTerminator (const AssaultTerminator& a)
{
    std::cout << "* teleports from space *" << std::endl;
    *this = a;
}

AssaultTerminator& AssaultTerminator::operator = (const AssaultTerminator& a)
{
    (void)a;
    return *this;
}

ISpaceMarine*   AssaultTerminator::clone() const
{
    ISpaceMarine* clone;

    clone = new AssaultTerminator(*this);
    return clone;
}

void            AssaultTerminator::battleCry() const
{
    std::cout << "This code is unclean. Purify it !" << std::endl;
}


void            AssaultTerminator::rangedAttack() const
{
    std::cout << "* does nothing *" << std::endl;
}

void            AssaultTerminator::meleeAttack() const
{
    std::cout << "* attaque with chainfists *" << std::endl;
}