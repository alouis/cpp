/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:17:28 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:17:29 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP

# include <iostream>
# include <string>
# include "ISquad.hpp"
# include "ISpaceMarine.hpp"

class Squad : public ISquad
{
    public:
        Squad();
        ~Squad();
        Squad (const Squad& s);
        Squad& operator = (const Squad& s);

        virtual int             getCount() const;
        virtual ISpaceMarine*   getUnit(int) const;
        virtual int             push(ISpaceMarine* marine);

    private:
        int             count;
        ISpaceMarine    **all_units;

        void            copySquad(const Squad& s);
        void            deleteSquad();
};

#endif