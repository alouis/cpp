/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:17:31 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:17:31 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"

TacticalMarine::TacticalMarine()
{
    std::cout << "Tactical Marine ready for action !" << std::endl;
}

TacticalMarine::~TacticalMarine()
{
    std::cout << "Aaargh ..." << std::endl;
}

TacticalMarine::TacticalMarine (const TacticalMarine& t)
{
    std::cout << "Tactical Marine ready for action !" << std::endl;
    *this = t;
}

TacticalMarine& TacticalMarine::operator = (const TacticalMarine& t)
{
    (void)t;
    return *this;
}

ISpaceMarine*   TacticalMarine::clone() const
{
    TacticalMarine* clone;
    
    clone = new TacticalMarine(*this);
    return clone;
}

void            TacticalMarine::battleCry() const
{
    std::cout << "For the Holy PLOT !" << std::endl;
}

void            TacticalMarine::rangedAttack() const
{
    std::cout << "* attacks with a bolter *" << std::endl;
}

void            TacticalMarine::meleeAttack() const
{
    std::cout << "* attacks with a chainsword *" << std::endl;
}