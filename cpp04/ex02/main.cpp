/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:17:23 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:20:23 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ISpaceMarine.hpp"
#include "TacticalMarine.hpp"
#include "AssaultTerminator.hpp"
#include "Squad.hpp"

int main()
{
    ISpaceMarine* bob = new TacticalMarine;
    ISpaceMarine* jim = new AssaultTerminator;
    ISpaceMarine* jane;
    ISpaceMarine* bill;
    ISpaceMarine* andy;
    ISpaceMarine* kevin;
    ISquad* vlc = new Squad;

    std::cout << std::endl << "New squad has " << vlc->getCount() << " marine" << std::endl << std::endl;
    jane = jim->clone();
    bill = bob->clone();
    andy = jane->clone();
    kevin = bill->clone();
    vlc->push(bob);
    vlc->push(jim);
    vlc->push(jane);
    vlc->push(bill);
    vlc->push(andy);
    vlc->push(kevin);
    std::cout << std::endl << "New squad has " << vlc->getCount() << " marines" << std::endl;
    std::cout << std::endl;
    for (int i = 0; i < vlc->getCount(); ++i)
    {
        ISpaceMarine* cur = vlc->getUnit(i);
        cur->battleCry();
        cur->rangedAttack();
        cur->meleeAttack();
        std::cout << std::endl;
    }
    delete vlc;

    return 0;
}