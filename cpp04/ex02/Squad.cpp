/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:17:25 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:17:26 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

Squad::Squad() : count(0), all_units(NULL)
{}

Squad::~Squad()
{
    deleteSquad();
}

Squad::Squad (const Squad& s) : count(0), all_units(NULL)
{
    copySquad(s);
}

Squad& Squad::operator = (const Squad& s)
{
    deleteSquad();
    copySquad(s);
    return *this;
}

void    Squad::copySquad(const Squad& oldSquad)
{
    if (oldSquad.getCount() > 0)
    {
        ISpaceMarine **new_units = new ISpaceMarine*[oldSquad.getCount()];
        for (int i = 0; i < oldSquad.getCount(); i++)
            new_units[i] = oldSquad.getUnit(i)->clone();
        this->all_units = new_units;
        this->count = oldSquad.getCount();
    }
}

void    Squad::deleteSquad()
{
    if (all_units)
    {
        for (int i = 0; i < getCount(); i++)
            delete all_units[i];
        delete[] all_units;
        all_units = NULL;
    }
    count = 0;
}

int             Squad::getCount() const
{
    return count;
}

ISpaceMarine*   Squad::getUnit(int i) const
{
    if (i >= 0 && i < count)
        return all_units[i];
    return NULL;
}

int             Squad::push(ISpaceMarine* marine)
{
    int i;

    if (!marine)
        return count;
    if (!all_units)
    {
        all_units = new ISpaceMarine*[1];
        all_units[0] = marine;
    }
    else
    {
        for (i = 0; i < count; i++)
        {
            if (all_units[i] == marine)
                return i;
        }
        ISpaceMarine **new_units = new ISpaceMarine*[i + 1];
        for (i = 0; i < count; i++)
            new_units[i] = all_units[i];
        new_units[i] = marine;
        delete[] all_units;
        all_units = new_units;
    }
    count++;
    return count;
}