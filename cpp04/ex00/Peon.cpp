/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:14 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:01:15 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "Peon.hpp"

Peon::Peon() {}

Peon::Peon(std::string chosen_name) : Victim(chosen_name)
{
    std::cout << "Zog zog." << std::endl;
}

Peon::~Peon()
{
    std::cout << "Bleuark..." << std::endl;
}

Peon::Peon (const Peon& p)
{
    *this = p;
}

Peon& Peon::operator = (const Peon &p)
{
    if (this != &p)
        this->name = p.name;
    return *this;
}

void    Peon::getPolymorphed() const
{
    std::cout << name << " was just polymorphed into a pink pony!" << std::endl;
}
