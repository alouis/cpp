/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:33 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 14:04:53 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP

# include <iostream>
# include <string>

class Victim
{
    public:
        Victim(std::string victim_name);
        virtual ~Victim();
        Victim (const Victim& v);
        Victim& operator=(const Victim& v);

        std::string     getName() const;
        virtual void    getPolymorphed() const;

    protected:
        Victim();
        std::string name;
};

std::ostream& operator<<(std::ostream& os, const Victim& v);

#endif