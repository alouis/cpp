/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:18 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:01:19 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP

# include <iostream>
# include <string>
# include "Victim.hpp"

class Peon : public Victim
{
    public:
        Peon(std::string chosen_name);
        ~Peon();
        Peon (const Peon& p);
        Peon& operator=(const Peon& p);

        virtual void getPolymorphed() const;

    private:
        Peon();
};

#endif