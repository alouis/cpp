/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: belloyandrea <belloyandrea@student.42.f    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:26 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 14:04:11 by belloyandre      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP

# include <iostream>
# include <string>
# include "Victim.hpp"

class Sorcerer
{
    public:
        Sorcerer(std::string chosen_name, std::string chosen_titre);
        ~Sorcerer();
        Sorcerer (const Sorcerer& s);
        Sorcerer& operator = (const Sorcerer& s);

        void    polymorph(Victim const &) const;
        std::string getName() const;
        std::string getTitle() const;
    
    private:
        Sorcerer();
        std::string name;
        std::string title;
};

std::ostream& operator<<(std::ostream& os, const Sorcerer& s);

#endif