/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:11 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:06:35 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"
#include "Endive.hpp"

int main()
{
    Sorcerer    sorcerer1("Merlin", "magician");
    Victim      victim1("Mickey");
    Peon        peon1("Mickael");
    Endive      endive1("Leon");

    std::cout << sorcerer1 << victim1 << peon1 << endive1;
    sorcerer1.polymorph(victim1);
    sorcerer1.polymorph(peon1);
    sorcerer1.polymorph(endive1);
    std::cout << std::endl << sorcerer1 << std::endl;
    victim1.getPolymorphed();
    peon1.getPolymorphed();
    endive1.getPolymorphed();
    return 0;
}