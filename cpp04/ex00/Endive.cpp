/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Endive.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:00:55 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:00:58 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "Endive.hpp"

Endive::Endive() {}

Endive::Endive(std::string chosen_name) : Victim(chosen_name)
{
    std::cout << "Endive " << name << " on its way" << std::endl;
}

Endive::~Endive()
{
    std::cout << "Pfffuuit..." << std::endl;
}

Endive::Endive (const Endive& p)
{
    *this = p;
}

Endive& Endive::operator = (const Endive &p)
{
    if (this != &p)
        this->name = p.name;
    return *this;
}

void    Endive::getPolymorphed() const
{
    std::cout << name << " was just polymorphed into a another type of vegetable!" << std::endl;
}