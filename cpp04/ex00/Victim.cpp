/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:30 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:01:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim() {}

Victim::Victim(std::string victim_name) : name(victim_name)
{
    std::cout << "Some random victim called " << name << " just appeared!" << std::endl;
}

Victim::~Victim()
{
    std::cout << "The victim " << name << " died for no apparent reasons!" << std::endl;
}

Victim::Victim (const Victim& v)
{
    *this = v;
}

Victim& Victim::operator=(const Victim& v)
{
    if (this != &v)
        this->name = v.name;
    return *this;
}

std::string Victim::getName() const
{
    return name;
}

void    Victim::getPolymorphed() const
{
    std::cout << name << " was just polymorphed in a cute little sheep!" << std::endl;
}

std::ostream& operator<<(std::ostream& os, const Victim& v)
{
    os << "I'm " << v.getName() << " and I like otters!" << std::endl;
    return os;
}