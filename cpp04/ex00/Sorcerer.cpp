/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:23 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:01:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "Sorcerer.hpp"

Sorcerer::Sorcerer(std::string chosen_name, std::string chosen_title) : name(chosen_name), title(chosen_title)
{
    std::cout << name << ", " << title << " is born !" << std::endl;
}

Sorcerer::~Sorcerer()
{
    std::cout << name << ", " << title << " is dead. Consequences will never be the same!" << std::endl;
}

Sorcerer::Sorcerer (const Sorcerer& s)
{
    *this = s;
}

Sorcerer&   Sorcerer::operator=(const Sorcerer& s)
{
    if (this != &s)
    {
        this->name = s.name;
        this->title = s.title;
    }
    return *this;
}

void    Sorcerer::polymorph(Victim const &v) const
{
    v.getPolymorphed();
}

std::string Sorcerer::getName() const
{
    return name;
}

std::string Sorcerer::getTitle() const
{
    return title;
}

std::ostream& operator<<(std::ostream& os, const Sorcerer& s)
{
    os << "I'm " << s.getName() << ", " << s.getTitle() << ", and I like ponies!" << std::endl;
    return os;
}