/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Endive.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:01:04 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:01:04 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENDIVE_HPP
# define ENDIVE_HPP

# include <iostream>
# include <string>
# include "Victim.hpp"

class Endive : public Victim
{
    public:
        Endive(std::string chosen_name);
        ~Endive();
        Endive (const Endive& p);
        Endive& operator=(const Endive& p);

        virtual void getPolymorphed() const;

    private:
        Endive();
};

#endif