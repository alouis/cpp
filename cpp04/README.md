## Polymorphisme et résolution statique/dynamique de liens

 * [Cours OpenClassroom](https://openclassrooms.com/fr/courses/1894236-programmez-avec-le-langage-c/1898632-mettez-en-oeuvre-le-polymorphisme)
 * [Shallow and deep copy](https://www.geeksforgeeks.org/shallow-copy-and-deep-copy-in-c/)
 * [Beware of circular dependency](https://stackoverflow.com/questions/37040693/c-why-is-there-unknown-type-when-class-header-is-included)
 * [Récapitulatif classes, pointeurs etc.](https://cdiese.fr/cpp-pointer-reference/)