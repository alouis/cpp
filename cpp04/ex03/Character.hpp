/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:37 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:22:37 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include <iostream>
# include <string>
# include "ICharacter.hpp"

class Character : public ICharacter
{
    public:
        Character(std::string name);
        ~Character();
        Character (const Character& ch);
        Character& operator = (const Character& ch);

        virtual std::string const & getName() const;
        virtual void                equip(AMateria* m);
        virtual void                unequip(int idx);
        virtual void                use(int idx, ICharacter& target);

    private:
        Character();
        std::string m_name;
        AMateria    *m_inventory[4];
        int         nb_materia;
};

#endif