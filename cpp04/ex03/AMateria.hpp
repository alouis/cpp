/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:41 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:22:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
# define AMATERIA_HPP

# include <iostream>
# include <string>
# include "ICharacter.hpp"

class ICharacter;

class AMateria
{
    public:
        AMateria(std::string const & type);
        virtual ~AMateria();
        AMateria (const AMateria& type);
        AMateria& operator = (const AMateria& type);

        std::string const & getType() const;
        unsigned int        getXP() const;
        virtual AMateria*   clone() const = 0;
        virtual void        use(ICharacter& target);

    protected:
        unsigned int        _xp;
        std::string const   m_type;

    private:
        AMateria();
};

#endif