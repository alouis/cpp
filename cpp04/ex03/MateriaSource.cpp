/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:19 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:22:20 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"

MateriaSource::MateriaSource() : nb_copies(0)
{
    for (int i = 0; i < 4; i++)
        m_copies[i] = 0;
}

MateriaSource::~MateriaSource()
{
    for (int i = 0; i < nb_copies; i++)
        delete m_copies[i];
}

MateriaSource::MateriaSource (const MateriaSource& ms)
{
    nb_copies = ms.nb_copies;
    for (int i = 0; i < 4; i++)
    {
        if (ms.m_copies[i] != 0)
            m_copies[i] = ms.m_copies[i]->clone();
        else
            m_copies[i] = 0;
    }
}

MateriaSource& MateriaSource::operator = (const MateriaSource& ms)
{
    nb_copies = ms.nb_copies;
    for (int i = 0; i < 4; i++)
    {
        if (m_copies[i] != 0)
            delete m_copies[i];
    }
    for (int i = 0; i < 4; i++)
    {
        if (ms.m_copies[i] != 0)
            m_copies[i] = ms.m_copies[i]->clone();
        else
            m_copies[i] = 0;
    }
    return *this;
}

void        MateriaSource::learnMateria(AMateria* m)
{
    if (nb_copies < 4 && m)
    {
        m_copies[nb_copies] = m;
        nb_copies += 1;
    }
}

AMateria*   MateriaSource::createMateria(std::string const & type)
{
    for (int i = 0; i < nb_copies; i++)
    {
        if (m_copies[i]->getType().compare(type) == 0)
            return m_copies[i]->clone();
    }
    return 0;
}