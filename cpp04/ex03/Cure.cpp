/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:35 by alouis            #+#    #+#             */
/*   Updated: 2021/05/28 14:14:54 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

Cure::Cure() : AMateria("cure")
{
}

Cure::~Cure() 
{
}

Cure::Cure (const Cure& c) : AMateria("cure")
{    
    this->_xp = c.getXP();
}

Cure& Cure::operator = (const Cure& c)
{
    this->_xp = c.getXP();
    return *this;
}

AMateria*   Cure::clone() const
{
    AMateria* new_materia;

    new_materia = new Cure(*this);
    return new_materia;
}

void        Cure::use(ICharacter& target)
{
    AMateria::use(target);
    std::cout << "* heals " << target.getName() << "'s wounds, xp = " << this->_xp << " *" << std::endl;
}