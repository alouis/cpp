/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:39 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:22:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() {}

Character::Character(std::string name) : m_name(name), nb_materia(0)
{
    for (int i = 0; i < 4; i++)
        m_inventory[i] = 0;
}

Character::~Character()
{
    if (nb_materia != 0)
    {
        for (int i = 0; i < nb_materia; i++)
        {
            if (m_inventory[i] != 0)
            {
                delete m_inventory[i];
                m_inventory[i] = 0;
            }
        }
        nb_materia = 0;
    }
}

Character::Character (const Character& ch)
{
    this->m_name = ch.getName();
    this->nb_materia = ch.nb_materia;
    for (int i = 0; i < nb_materia; i++)
    {
        if (ch.m_inventory[i])
            this->m_inventory[i] = ch.m_inventory[i]->clone();
        else
            this->m_inventory[i] = 0;
    }
}

Character& Character::operator = (const Character& ch)
{
    this->m_name = ch.getName();
    this->nb_materia = ch.nb_materia;
    for (int i = 0; i < nb_materia; i++)
    {
        if (this->m_inventory[i])
            delete this->m_inventory[i];
    }
    for (int i = 0; i < nb_materia; i++)
    {
        if (ch.m_inventory[i])
            this->m_inventory[i] = ch.m_inventory[i]->clone();
        else
            this->m_inventory[i] = 0;
    }
    return *this;
}

std::string const & Character::getName() const
{
    return m_name;
}

void                Character::equip(AMateria* m)
{
    int i;

    if (!m || nb_materia == 4)
        return ;
    for (i = 0; i < nb_materia; i++)
    {
        if (m_inventory[i] == m)
            return ;
    }
    m_inventory[i] = m;
    nb_materia += 1;
    std::cout << "Equiped inventory with a new arm : now " << nb_materia << " in stock" << std::endl;
}

void                Character::unequip(int idx)
{
    int j;

    j = 0;
    if (nb_materia == 0 || idx >= nb_materia || idx < 0)
        return ;
    for (int i = idx; i < nb_materia; i++)
        m_inventory[i] = m_inventory[i + 1];
    nb_materia -= 1;
}

void                Character::use(int idx, ICharacter& target)
{
    if (nb_materia != 0 && idx >= 0 && idx < nb_materia)
        m_inventory[idx]->use(target);
}