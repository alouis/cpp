#include "AMateria.hpp"

AMateria::AMateria() : _xp(0), m_type(0)
{}

AMateria::AMateria(std::string const & type) : _xp(0), m_type(type)
{}

AMateria::~AMateria() {}

AMateria::AMateria (const AMateria& type)
{
    *this = type;
}

AMateria& AMateria::operator = (const AMateria& type)
{
    if (this != &type)
        this->_xp = type.getXP();
    return *this;
}

std::string const & AMateria::getType() const
{
    return m_type;
}


unsigned int        AMateria::getXP() const
{
    return _xp;
}

void                AMateria::use(ICharacter& target)
{
    (void)target;
    _xp += 10;
}