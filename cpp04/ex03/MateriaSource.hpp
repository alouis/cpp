/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:16 by alouis            #+#    #+#             */
/*   Updated: 2021/04/19 12:22:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATERIASOURCE_HPP
# define MATERIASOURCE_HPP

# include <iostream>
# include <string>
# include "IMateriaSource.hpp"

class MateriaSource : public IMateriaSource
{
    public:
        MateriaSource();
        ~MateriaSource();
        MateriaSource (const MateriaSource& ms);
        MateriaSource& operator = (const MateriaSource& ms);

        virtual void        learnMateria(AMateria*);
        virtual AMateria*   createMateria(std::string const & type);

    private:
        AMateria*   m_copies[4];
        int         nb_copies;
};

#endif