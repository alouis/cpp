/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:21 by alouis            #+#    #+#             */
/*   Updated: 2021/05/28 14:18:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include "ICharacter.hpp"
#include "Character.hpp"

int main()
{
    AMateria* ice = new Ice();
    AMateria* copy_ice;
    AMateria* cure = new Cure();
    AMateria* copy_cure;
    AMateria* snd_copy;

    ICharacter* billy = new Character("billy");

    std::cout << std::endl << billy->getName() << " : equip/unequip" << std::endl;
    billy->unequip(0);
    billy->equip(ice);
    billy->equip(cure);
    copy_ice = ice->clone();
    billy->equip(copy_ice);
    copy_cure = cure->clone();
    billy->equip(copy_cure);
    snd_copy = cure->clone();
    billy->equip(snd_copy);
    billy->unequip(4);
    billy->equip(snd_copy);

    std::cout << std::endl << billy->getName() << " : uses AMaterial 0 to 3 on himself" <<std::endl;
    billy->use(0, *billy);
    billy->use(1, *billy);
    billy->use(1, *billy);
    billy->use(1, *billy);
    billy->use(1, *billy);
    billy->use(2, *billy);
    billy->use(3, *billy);
    delete billy;
    return 0;
}