/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/19 12:22:29 by alouis            #+#    #+#             */
/*   Updated: 2021/05/28 14:15:10 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"

Ice::Ice() : AMateria("ice")
{
}

Ice::~Ice() 
{
}

Ice::Ice (const Ice& i) : AMateria("ice")
{
    _xp = i._xp;
}

Ice& Ice::operator = (const Ice& i)
{
    _xp = i._xp;
    return *this;
}

AMateria*   Ice::clone() const
{
    AMateria* new_materia;

    new_materia = new Ice(*this);
    return new_materia;
}

void        Ice::use(ICharacter& target)
{
    AMateria::use(target);
    std::cout << "* shoots an ice bolt at " << target.getName() << ", xp = " << this->_xp << " *" << std::endl;
}