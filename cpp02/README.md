### Classes canoniques

## Overload
  * [Explications (FR)](https://codes-sources.commentcamarche.net/source/10604-classe-canonique-ni-fuite-memoire-ni-trap)
  * [Overloaded assigment](https://www.learncpp.com/cpp-tutorial/overloading-the-assignment-operator/)
  * [Overloading the comparison operators](https://www.learncpp.com/cpp-tutorial/overloading-the-comparison-operators/)
  * [Overloading decrement and increment operators](https://docs.microsoft.com/en-us/cpp/cpp/increment-and-decrement-operator-overloading-cpp?view=msvc-160)
  * [Operators overloaded](https://en.cppreference.com/w/cpp/language/operators)
  * [ostream operator](https://docs.microsoft.com/en-us/cpp/standard-library/overloading-the-output-operator-for-your-own-classes?view=msvc-160)

 ## Bits Shifts

  * [Shift operators in C++](https://www.geeksforgeeks.org/left-shift-right-shift-operators-c-cpp/)