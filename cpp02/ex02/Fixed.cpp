/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 10:04:36 by alouis            #+#    #+#             */
/*   Updated: 2021/04/08 10:08:15 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

Fixed::Fixed () : fixedPointNbr(0)
{
    std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed (const int v_int)
{
    std::cout << "Int constructor called" << std::endl;

    fixedPointNbr = v_int << bitsSize;
}

Fixed::Fixed (const float v_float)
{
    std::cout << "Float constructor called" << std::endl;

    fixedPointNbr = roundf(v_float * (1 << bitsSize));
}

Fixed::~Fixed() 
{
    std::cout << "Destructor called" << std::endl;
}

Fixed::Fixed(const Fixed& f)
{
    std::cout << "Copy constructor called" << std::endl;

    *this = f;
}

Fixed& Fixed::operator = (const Fixed& f)
{
    std::cout << "Assignation operator called" << std::endl;

    if (this != &f)
        this->fixedPointNbr = f.getRawBits();
    return *this;
}

int Fixed::getRawBits() const
{
    std::cout << "getRawBits member function called" << std::endl;

    return fixedPointNbr;
}

void Fixed::setRawBits(int const raw)
{
    fixedPointNbr = raw;
}

int Fixed::toInt(void) const
{
    int binaryNbr;

    binaryNbr = fixedPointNbr >> bitsSize;
    return binaryNbr;
}

float Fixed::toFloat(void) const
{
    float   binaryNbr;

	binaryNbr = (float)fixedPointNbr / (1 << bitsSize);
    return binaryNbr;
}

std::ostream& operator<<(std::ostream& os, const Fixed& f)
{
    os << f.toFloat();
    return os;
}

/*
**	---Overloaded comparison operators---
*/

bool    Fixed::operator== (const Fixed &nbrComp)
{
    if (this->fixedPointNbr == nbrComp.fixedPointNbr)
        return true;
    return false;
}

bool    Fixed::operator!= (const Fixed &nbrComp)
{
    if (this->fixedPointNbr != nbrComp.fixedPointNbr)
        return true;
    return false;
}

bool    Fixed::operator<= (const Fixed &nbrComp)
{
    if (this->fixedPointNbr <= nbrComp.fixedPointNbr)
        return true;
    return false;
}

bool    Fixed::operator>= (const Fixed &nbrComp)
{
    if (this->fixedPointNbr <= nbrComp.fixedPointNbr)
        return true;
    return false;
}

bool    Fixed::operator< (const Fixed &nbrComp) const
{
    if (this->fixedPointNbr < nbrComp.fixedPointNbr)
        return true;
    return false;
}

bool    Fixed::operator> (const Fixed &nbrComp) const
{
    if (this->fixedPointNbr > nbrComp.fixedPointNbr)
        return true;
    return false;
}


/*
**	---Overloaded mathematic operators---
*/
Fixed   Fixed::operator+ (const Fixed &n1) const
{
    Fixed newObj;
    int newNbr;

    newNbr = this->fixedPointNbr + n1.getRawBits();
    newObj.setRawBits(newNbr);
    return newObj;
}

Fixed   Fixed::operator- (const Fixed &n1) const
{
    Fixed newObj;
    int newNbr;

    newNbr = this->fixedPointNbr - n1.getRawBits();
    newObj.setRawBits(newNbr);
    return newObj;
}

Fixed   Fixed::operator* (const Fixed &n1) const
{
    Fixed newObj;
    int newNbr;

    newNbr = this->fixedPointNbr * n1.getRawBits() >> this->bitsSize;
    newObj.setRawBits(newNbr);
    return newObj;
}

Fixed   Fixed::operator/ (const Fixed &n1) const
{
    Fixed newObj;
    int newNbr;

    newNbr = this->toFloat() / n1.toFloat() * (1 << this->bitsSize);
    newObj.setRawBits(newNbr);
    return newObj;
}


/*
**	---Prefix and postfix increment and decrement operators---
*/
Fixed& Fixed::operator++()
{
   this->fixedPointNbr++;
   return *this;
}

Fixed Fixed::operator++(int)
{
   Fixed temp = *this;
   ++*this;
   return temp;
}

Fixed& Fixed::operator--()
{
   this->fixedPointNbr--;
   return *this;
}

Fixed Fixed::operator--(int)
{
   Fixed temp = *this;
   --*this;
   return temp;
}

Fixed const& min(Fixed const& firstInstance, Fixed const& secondInstance)
{
    return (Fixed::min(firstInstance, secondInstance));
}

Fixed const& Fixed::min(Fixed const& firstInstance, Fixed const& secondInstance)
{
    if (firstInstance < secondInstance)
        return (firstInstance);
    return (secondInstance);
}

Fixed const& max(Fixed const& firstInstance, Fixed const& secondInstance)
{
    return (Fixed::max(firstInstance, secondInstance));
}

Fixed const& Fixed::max(Fixed const& firstInstance, Fixed const& secondInstance)
{
    if (firstInstance > secondInstance)
        return (firstInstance);
    return (secondInstance);
}