/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 10:04:40 by alouis            #+#    #+#             */
/*   Updated: 2021/04/08 10:04:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>
# include <cmath>

class Fixed
{
    public:
        //Constructors
        Fixed();
        Fixed(const int v_int);
        Fixed(const float v_float);

        //Destructors
        ~Fixed();

        //Copy constructor
        Fixed (const Fixed& f);

        //Overloaded assigment
        Fixed& operator = (const Fixed& f);

        //Overloaded comparison operators
        bool    operator== (const Fixed &nbrComp);
        bool    operator!= (const Fixed &nbrComp);
        bool    operator<= (const Fixed &nbrComp);
        bool    operator>= (const Fixed &nbrComp);
        bool    operator< (const Fixed &nbrComp) const;
        bool    operator> (const Fixed &nbrComp) const;

        //Overloaded arithmetic operators
        Fixed   operator+ (const Fixed &n1) const;
        Fixed   operator- (const Fixed &n1) const;
        Fixed   operator* (const Fixed &n1) const;
        Fixed   operator/ (const Fixed &n1) const;

        // Prefix and postfix increment and decrement operators
        Fixed&  operator++();        // Prefix increment operator.
        Fixed   operator++(int);     // Postfix increment operator.
        Fixed&  operator--();        // Prefix decrement operator.
        Fixed   operator--(int);     // Postfix decrement operator.

        int     getRawBits(void) const;
        void    setRawBits(int const raw);
        int     toInt(void) const;
        float   toFloat(void) const;

        static  const Fixed& min(Fixed const& nb1, Fixed const& nb2);
        static  const Fixed& max(Fixed const& nb1, Fixed const& nb2);

    private:
        int                 fixedPointNbr;
        static const int    bitsSize = 8;
};

std::ostream& operator<<(std::ostream& os, const Fixed& f);
Fixed const& min(Fixed const& nb1, Fixed const& nb2);
Fixed const& max(Fixed const& nb1, Fixed const& nb2);

#endif