/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 10:04:38 by alouis            #+#    #+#             */
/*   Updated: 2021/04/08 10:04:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

int main(void) 
{ 
    Fixed a;
    Fixed const b(Fixed(5.05f) / Fixed(2));

    std::cout << a << std::endl;
    std::cout << ++a << std::endl;
    std::cout << a << std::endl;
    std::cout << a++ << std::endl;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << min(a, b) << std::endl;
    std::cout << Fixed::min(a, b) << std::endl;
    std::cout << max(a, b) << std::endl;
    std::cout << Fixed::max(a, b) << std::endl;
    return 0;
}