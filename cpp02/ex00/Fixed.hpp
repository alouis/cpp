/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 09:57:23 by alouis            #+#    #+#             */
/*   Updated: 2021/04/08 09:57:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>

class Fixed
{
    public:
        //Constructor
        Fixed();
        //Destructor
        ~Fixed();
        //Copy constructor
        Fixed (const Fixed& f);
        //Assigment operator
        Fixed& operator = (const Fixed& f);

        int     getRawBits(void) const;
        void    setRawBits(int const raw);

    private:
        int                 value;
        static const int    fract_bits = 8;
};

#endif