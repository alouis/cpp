/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 10:03:04 by alouis            #+#    #+#             */
/*   Updated: 2021/04/08 10:03:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>
# include <cmath>

class Fixed
{
    public:
        Fixed();
        Fixed(const int v_int);
        Fixed(const float v_float);
        ~Fixed();
        Fixed (const Fixed& f);
        Fixed& operator = (const Fixed& f);

        int     getRawBits(void) const;
        void    setRawBits(int const raw);
        int     toInt(void) const;
        float   toFloat(void) const;

    private:
        int                 fixedPointNbr;
        static const int    bitsSize = 8;
};

std::ostream& operator<<(std::ostream& os, const Fixed& f);

#endif