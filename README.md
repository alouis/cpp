### Subjects

- Cpp00: Namespace, class, member functions, stdio stream, initialization lists, static, const, etc.  

- Cpp01: Allocation mémoire, réference, pointeur membres, file streams  

- Cpp02: Polymorphisme ad-hoc, overloads et classes canoniques   

- Cpp03: Héritage   

- Cpp04: Polymorphisme sous-type, classes abstraites, interfaces   

- Cpp05: Répétition et Exceptions   

- Cpp06: Casts   

- Cpp07: Templates

- Cpp08: Templated containers, iterators, algorithms
